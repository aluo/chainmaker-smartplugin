/*
 *
 *  Copyright (C) THL A29 Limited, a Tencent company. All rights reserved.
 *  SPDX-License-Identifier: Apache-2.0
 *
 */

const merge = require("webpack-merge");
const common = require("./webpack.common.js");
// const ExtensionReloader = require('webpack-extension-reloader');
const WebExtension = require('webpack-target-webextension');

module.exports = (env = {}) => {
  return merge(common(env), {
    mode: "development", devtool: "inline-source-map", plugins: (env.reload === 'true' ? 
    // [new ExtensionReloader({
    //   port: 9090, // Which port use to create the server
    //   reloadPage: false, // Force the reload of the page also
    //   entries: {
    //     popup: "popup", background: "eventPage", contentScript: "contentScript"
    //   }
    // })] 
    [new WebExtension({
      background: {
        entry: 'eventPage',
        // !! Add this to support manifest v3
        manifest: 3,
      },
    })]
    : [])
  });
};
