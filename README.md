> ChainMaker钱包插件，插件支持的是底链V2.1版本即是2.x以上

[TOC]

[![](./deploy/doc/chrome-store.png)](https://chrome.google.com/webstore/detail/chainmaker-smartplugin/ekjipgejfjfhjingjpgbmkcppnpfdggc?utm_source=chrome-ntp-icon)

## 项目介绍

```
├── src #插件
├── web-sdk
│   ├── chainmaker-pb-files #PB文件生成的JS-只读文件
│   ├── sdk #长安链Web SDK
```

## 开发

```shell
# 安装包
$ npm install

# 开发构建
$ npm run watch

# 打包
$ npm run build

```

添加节点，test中提供测试节点数据，使用前可使用`telnet 192.168.1.108 16301`测试联通性


## 可行性介绍

1. 基于[NodeJS SDK](https://git.chainmaker.org.cn/chainmaker/sdk-nodejs) 版进行改造，封装为WebSDK
2. 对于gRPC的支持，相关包是`google-protobuf，grpc-web`
    - grpc-web为客户端提供协议支持
    - google-protobuf为JS版提供格式支持

## 相关文档

- [原型设计](https://modao.cc/app/631f0aea6145369ab194517765cad7dbcaab873e)
- [UI-蓝狐设计](https://lanhuapp.com/web/?code=011PgE0005VGcN1GQ6100keQjy3PgE0h#/item/project/stage?tid=8224e230-c931-4c77-b70a-86d22936163b&pid=802b1b85-da5d-4c8e-8514-f3450658a357)

## 使用手册

见[HANDBOOK.md](./HANDBOOK.md)
