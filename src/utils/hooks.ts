/*
 *
 *  Copyright (C) THL A29 Limited, a Tencent company. All rights reserved.
 *  SPDX-License-Identifier: Apache-2.0
 *
 */

import { useEffect, useState } from 'react';
import { Account, Chain, TxLog } from './interface';
import chainStorageUtils from './storage';

export function useAccounts(chainId: Chain['chainId']) {
  const [accounts, setAccounts] = useState<Account[]>([]);

  useEffect(() => {
    chainId && chainStorageUtils.getChainAccounts(chainId).then(res => {
      setAccounts(res);
    });
  }, [chainId]);
  return accounts;
}


export function useTxLogs(chainSelected: Chain['chainId']) {
  const [logs, setLogs] = useState<TxLog[]>([]);
  useEffect(() => {
    chainSelected && chainStorageUtils.getTxLogs(chainSelected).then(res => {
      // const map = new Array(30).fill(null).map((item, index) => ({
      //   ...res[0],
      //   txId: String(index),
      // }));
      setLogs(res);
    });
  }, [chainSelected]);

  return {
    logs, 
    setLogs: (logs:TxLog[])=>{
      chainStorageUtils.updateTxLogs(chainSelected,logs).then(res=>{
        setLogs(logs);
      })
    }
  }
}

