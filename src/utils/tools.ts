import { ExtensionResponse } from "../event-page";
import chainStorageUtils from "./storage";

/**
 * 发布消息到网页的contentScript
 * @see https://developer.chrome.com/docs/extensions/reference/tabs/#method-sendMessage
 */

// TODO: 会触发window.addEventListener('message',... )
 export function sendMessageToContentScript(message: ExtensionResponse) {
    chainStorageUtils.getActiveTabId().then(tabId => {
        if(!tabId){
            return;
        }
        chrome.tabs.sendMessage(tabId, {
            ...message,
            from: 'chainmaker-plugin'
        });
    });
  }


export const getNowSeconds = () => Math.ceil(Date.now() / 1000);