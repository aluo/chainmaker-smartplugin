import React from 'react';
import { Copy, Form, StyledProps } from 'tea-component';

import './text-info.less';
export interface TextInfoItem extends StyledProps {
  label?: React.ReactNode;
  value: string | number;
  theme?: string;
  copyable?: boolean;
  href?: string;
}
export interface TextInfoProps extends StyledProps {
  sourceData: TextInfoItem[];
  dense?: boolean;
  lableBold?: boolean;
}
export function TextInfo({ sourceData, lableBold, className, dense, ...props }: TextInfoProps) {
  return (
    <div className={`text-info-list ${lableBold ? 'text-info-dense' : ''} ${className || ''}`} {...props}>
      {sourceData.map((ele, index) => {
        const { value, label, copyable, className = '', style, href } = ele;
        return value || value === 0 ? (
          <div key={index} className={`text-info-item ${className}`} style={style}>
            <div className="text-info-label txs-h3" style={{ fontWeight: lableBold ? 'bold' : 'normal' }}>
              {label}
            </div>
            <div className="text-info-value">
              {href ? (
                <a className="text-blue" href={href} target="_blank" rel="noreferrer">
                  {value}
                </a>
              ) : (
                <span>{value}</span>
              )}{' '}
              {copyable && value && <Copy text={String(value)} />}
            </div>
          </div>
        ) : null;
      })}
    </div>
  );
}

export interface TextSectionInfoProps extends TextInfoProps {
  title?: string;
  lineBreak?: boolean;
}

export function TextSectionInfo({
  sourceData,
  className,
  dense,
  lableBold,
  lineBreak,
  title,
  ...props
}: TextSectionInfoProps) {
  return (
    <div className={`text-section-info ${className || ''}`} {...props}>
      {title ? (
        <div className="text-info-label txs-h3" style={{ fontWeight: 'bold' }}>
          {title}
        </div>
      ) : null}

      {/* <div className={`text-info-list ${lableBold ? 'text-info-dense' : ''}`}>
        {sourceData.map((ele, index) => {
          const { value, label, copyable, className = '', style, href } = ele;
          return value || value === 0 ? (
            <div key={index} className={`text-info-item ${className}`} style={style}>
              <span className="text-info-label txs-h3" style={{ fontWeight: lableBold ? 'bold' : 'normal' }}>
                {label}
              </span>
              <span className="text-info-value">
                {href ? (
                  <a className="text-blue" href={href} target="_blank" rel="noreferrer">
                    {value}
                  </a>
                ) : (
                  value
                )}{' '}
                {copyable && value && <Copy text={String(value)} />}
              </span>
            </div>
          ) : null;
        })}
      </div> */}

      <Form readonly>
        {sourceData.map((ele, index) => {
          const { value, label } = ele;
          return (
            <Form.Item key={index} label={label}>
              <Form.Text>{value}</Form.Text>
            </Form.Item>
          );
        })}
      </Form>
    </div>
  );
}
