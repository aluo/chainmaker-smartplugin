/*
 *
 *  Copyright (C) THL A29 Limited, a Tencent company. All rights reserved.
 *  SPDX-License-Identifier: Apache-2.0
 *
 */

import React, { useCallback, useEffect, useMemo, useRef, useState } from 'react';
import { ConfirmModal } from '../utils/common';
import { Button, Form, Input, message } from 'tea-component';
import { Controller, useForm } from 'react-hook-form';
import formUtils, { Validator } from '../utils/form-utils';
import { UploadFile } from '../utils/upload-file';
import { Account, AccountForm, Chain, ChainForm } from '../utils/interface';
import chainStorageUtils from '../utils/storage';

import { createPemFile, getChainVersion, isLackTLSChain, trimFileNameSuffix, updateChainConfig } from '../utils/utils';

import { useAccounts } from '../utils/hooks';
import GlossaryGuide from '../utils/glossary-guide';
import { useChainStore } from '../popup/popup';
import { createCrtName } from '../popup/chain-new-page';
import { getAddressFromKeyPemFile, file2Txt } from '../../web-sdk/glue';
import { isOfficialChain } from '../utils/official-chain';
import { initChainSubscribe } from '../services/chain';
// @ts-ignore
import chainmakerWalletJSSDK from 'chainmaker-wallet-jssdk';
import { updateAccountDidAndVc } from '../services/did';
const { tools } = chainmakerWalletJSSDK;

interface AccountNewProps {
  chain: Chain;
  initial?: boolean;
  defaultValues?: AccountForm;
  onSuccess: () => void;
  onError: (err: Error) => void;
  onCancel: () => void;
}

function AccountNew({ chain, defaultValues, onSuccess, onError, onCancel }: AccountNewProps) {
  const {
    control,
    reset,
    formState: { isValidating, isSubmitted, isValid },
    getValues,
    setValue,
  } = useForm({
    mode: 'onBlur',
  });

  const httpFailRef = useRef<typeof ConfirmModal>();
  const chainFailRef = useRef<typeof ConfirmModal>();
  const refSignKey = useRef(null);
  const refPublicKey = useRef(null);
  const refSignCert = useRef(null);
  const [loading, setLoading] = useState(false);
  const { setChains, setSelectedChain, currentAccount, setCurrentAccount } = useChainStore();
  // 异常处理
  useMemo(() => {
    if (!chain) {
      const msg = '无效的chainId，请先添加链配置';
      message.error({
        content: msg,
      });
      onError(new Error(msg));
    }
  }, []);

  const lackTLS = useMemo(() => isLackTLSChain(chain), [chain]);
  const accounts = useAccounts(chain?.chainId);
  const handleCancel = useCallback(() => {
    onCancel?.();
  }, []);

  // 设置默认值
  useEffect(() => {
    // 修改选中链
    setSelectedChain(chain);
    if (defaultValues) {
      // 回显
      refSignKey.current?.setDefaultValue(defaultValues.userSignKeyFile);
      refSignCert.current?.setDefaultValue(defaultValues.userSignCrtFile);
      refPublicKey.current?.setDefaultValue(defaultValues.userPublicKeyFile);
      setValue('orgId', defaultValues.orgId, {
        shouldValidate: true,
      });
      setValue('name', defaultValues.name, {
        shouldValidate: true,
      });
    } else if (accounts.length) {
      reset({
        orgId: accounts[0].orgId,
      });
    }
  }, []);

  const handleClick = useCallback(async () => {
    setLoading(true);
    try {
      if (chain.accountMode === 'public') {
        // 公钥模式，只有私钥，没有公钥，则自动推导出公钥文件
        const { userPublicKeyFile, userSignKeyFile, name } = getValues();
        if (!userPublicKeyFile && userSignKeyFile) {
          // 私钥推导出公钥
          const priPem = await file2Txt(userSignKeyFile);
          const pubPem = tools.handler.priKey2PubKey(priPem);
          const userPublicKeyName = `${name}.pem`;
          const userPublicKeyFile = createPemFile(pubPem, userPublicKeyName, 'application/x-x509-ca-cert');
          setValue('userPublicKeyFile', userPublicKeyFile);
        }
      }
      const values = getValues() as ChainForm & AccountForm;
      const [userSignKeyFile, userSignCrtFile, userPublicKeyFile, userTLSKeyFilePath, userTLSCrtFilePath] =
        await chainStorageUtils.uploadFiles([
          values.userSignKeyFile,
          values.userSignCrtFile,
          values.userPublicKeyFile,
          values.userTLSKeyFile,
          values.userTLSCrtFile,
        ]);
      // 未链接的http链验证
      if (chain.protocol === 'HTTP' && !chain.version) {
        try {
          console.debug('检测是否支持http', chain.nodeIp);
          await getChainVersion(`http${chain.tlsEnable ? 's' : ''}://${chain.nodeIp}`);
        } catch (e) {
          console.debug('getChainVersion error', e);
          if (e.code === 0 && chain.tlsEnable) {
            // @ts-ignore
            httpFailRef.current.show();
            return;
          }
          if (!isOfficialChain(chain)) {
            // @ts-ignore
            chainFailRef.current.show();
            setLoading(false);
            return;
          }
        }
      }
      // 构造account
      const address = await getAddressFromKeyPemFile(values.userSignKeyFile);
      const account: Account =
        chain.accountMode === 'public'
          ? {
            userSignKeyFile,
            userPublicKeyFile,
            name: values.name || trimFileNameSuffix(values.userSignKeyFile.name),
            crtName: await createCrtName(values, chain.accountMode),
            address,
          }
          : {
            userSignKeyFile,
            userSignCrtFile,
            crtName: await createCrtName(values, values.accountMode),
            // 默认为证书文件名
            name: values.name || trimFileNameSuffix(values.userSignCrtFile.name),
            orgId: values.orgId,
            address,
          };

      // 未链接的链。链接并更新信息
      if (!chain.version) {
        const res = await updateChainConfig({ ...chain, userTLSKeyFilePath, userTLSCrtFilePath }, values, lackTLS);
        if (res) {
          setSelectedChain(res.updatedChain);
          setChains(res.chains);
          await chainStorageUtils.setSelectedChain(res.updatedChain);
          await initChainSubscribe(res.updatedChain);
        } else {
          message.error({
            content: '区块链网络连接失败',
            duration: 5000,
          });
          setLoading(false);
          return;
        }
      }
      const existAccount = await chainStorageUtils.checkChainAccountExist(chain.chainId, account);
      if (existAccount) {
        message.error({
          content: '链账户已存在',
        });
        setLoading(false);
      } else {
        // 更新账户
        await chainStorageUtils.addChainAccount(chain.chainId, account);
        setLoading(false);
        if (!currentAccount) {
          chainStorageUtils.setCurrentAccount(account.address).then((res) => {
            setCurrentAccount(res);
          });
        }
        // 获取did，vc信息
        await updateAccountDidAndVc({ chainId: chain.chainId, account });

        onSuccess?.();
      }
    } catch (e) {
      setLoading(false);
      message.error({
        content: `添加区块链账户失败\n${e.message || e}`,
        duration: 5000,
      });
    }
  }, []);

  if (!chain) return null;
  return (
    <>
      <Form layout={'vertical'}>
        <Controller
          control={control}
          name="name"
          render={({ field, fieldState }) => (
            <Form.Item
              status={formUtils.getStatus({
                fieldState,
                isValidating,
                isSubmitted,
              })}
              label={<GlossaryGuide title={'账户名称'} />}
            >
              <Input size={'full'} {...field} />
            </Form.Item>
          )}
        />
        {chain.accountMode === 'permissionedWithCert' && (
          <Controller
            control={control}
            rules={{
              validate: Validator.validateOrgId,
            }}
            name="orgId"
            render={({ field, fieldState }) => (
              <Form.Item
                status={formUtils.getStatus({
                  fieldState,
                  isValidating,
                  isSubmitted,
                })}
                message={fieldState.error?.message}
                label="用户所在组织ID"
              >
                <Input size={'full'} {...field} />
              </Form.Item>
            )}
          />
        )}

        {chain.accountMode === 'permissionedWithCert' && (
          <Controller
            control={control}
            rules={{
              required: '请输入',
            }}
            name="userSignCrtFile"
            render={({ field, fieldState }) => (
              <Form.Item
                status={formUtils.getStatus({
                  fieldState,
                  isValidating,
                  isSubmitted,
                })}
                label="用户签名证书"
              >
                <UploadFile
                  {...field}
                  ref={refSignCert}
                  accept=".crt"
                  onSuccess={(key: File | null) => {
                    setValue(field.name, key, {
                      shouldValidate: true,
                    });
                  }}
                />
              </Form.Item>
            )}
          />
        )}
        <Controller
          control={control}
          rules={{
            required: '请输入',
          }}
          name="userSignKeyFile"
          render={({ field, fieldState }) => (
            <Form.Item
              status={formUtils.getStatus({
                fieldState,
                isValidating,
                isSubmitted,
              })}
              label="用户签名私钥"
            >
              <UploadFile
                {...field}
                ref={refSignKey}
                accept=".key"
                onSuccess={(key: File | null) => {
                  setValue(field.name, key, {
                    shouldValidate: true,
                  });
                }}
              />
            </Form.Item>
          )}
        />
        {chain.accountMode === 'public' && (
          <div style={{ display: 'none' }}>
            <Controller
              control={control}
              name="userPublicKeyFile"
              render={({ field, fieldState }) => (
                <Form.Item
                  status={formUtils.getStatus({
                    fieldState,
                    isValidating,
                    isSubmitted,
                  })}
                  label="用户公钥"
                >
                  <UploadFile
                    {...field}
                    accept=".key,.pem"
                    ref={refPublicKey}
                    onSuccess={(key: File | null) => {
                      setValue(field.name, key, {
                        shouldValidate: true,
                      });
                    }}
                  />
                </Form.Item>
              )}
            />
          </div>
        )}

        {lackTLS ? (
          <>
            <Controller
              control={control}
              rules={{
                required: '请输入',
              }}
              name="userTLSCrtFile"
              render={({ field, fieldState }) => (
                <Form.Item
                  status={formUtils.getStatus({
                    fieldState,
                    isValidating,
                    isSubmitted,
                  })}
                  label="用户TLS证书"
                >
                  <UploadFile
                    {...field}
                    accept=".crt"
                    onSuccess={(key: File | null) => {
                      setValue(field.name, key, {
                        shouldValidate: true,
                      });
                    }}
                  />
                </Form.Item>
              )}
            />
            <Controller
              control={control}
              rules={{
                required: '请输入',
              }}
              name="userTLSKeyFile"
              render={({ field, fieldState }) => (
                <Form.Item
                  status={formUtils.getStatus({
                    fieldState,
                    isValidating,
                    isSubmitted,
                  })}
                  label="用户TLS私钥"
                >
                  <UploadFile
                    {...field}
                    accept=".key"
                    onSuccess={(key: File | null) => {
                      setValue(field.name, key, {
                        shouldValidate: true,
                      });
                    }}
                  />
                </Form.Item>
              )}
            />
          </>
        ) : null}
      </Form>
      <footer className={'mt-12n btn-group'}>
        <Button type={'weak'} onClick={handleCancel}>
          取消
        </Button>
        <Button
          type={'primary'}
          disabled={!isValid}
          onClick={() => {
            handleClick();
          }}
          loading={loading}
        >
          确认添加
        </Button>
      </footer>
      <ConfirmModal title={'系统提示'} ref={httpFailRef} isHideConfirmBtn={true} cancelBtnText={'关闭'}>
        {'https请求失败，请检测tls证书是否被信任'}
      </ConfirmModal>
      <ConfirmModal title={'系统提示'} ref={chainFailRef} isHideConfirmBtn={true} cancelBtnText={'关闭'}>
        {'添加链配置失败，请检测长安链节点配置是否支持HTTP协议、TLS验证模式是否配置正确'}
      </ConfirmModal>
    </>
  );
}

export default AccountNew;
