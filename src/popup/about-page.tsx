/*
 *
 *  Copyright (C) THL A29 Limited, a Tencent company. All rights reserved.
 *  SPDX-License-Identifier: Apache-2.0
 *
 */

import React from 'react';
import { DetailPage } from '../utils/common';

function AboutPage() {
  return (
    <DetailPage title={'关于我们'} backUrl={'/'}>
      <h3>长安链介绍</h3>
      <div className={'tea-mt-2n'}>
        “长安链·ChainMaker”具备自主可控、灵活装配、软硬一体、开源开放的突出特点，由北京微芯研究院、清华大学、北京航空航天大学、
        腾讯、百度和京东等知名高校、企业共同研发。取名“长安链”，喻意“长治久安、再创辉煌、链接世界”。
      </div>
      <h3 className={'mt-8n'}>SmartPlugin介绍</h3>
      <div className={'tea-mt-2n'}>
        ChainMaker SmartPlugin由长安链官方团队研发，使得用户在使用Web3应用时可直接通过SmartPlugin与长安链进行交互。
      </div>
      <h3 className={'mt-8n'}>长安链官网</h3>
      <div className={'tea-mt-2n'}>
        <a href={'https://www.chainmaker.org.cn'} target="_blank" rel="noreferrer">
          https://www.chainmaker.org.cn
        </a>
      </div>
    </DetailPage>
  );
}

export default AboutPage;
