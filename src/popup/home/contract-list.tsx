import React, { CSSProperties, memo, useEffect, useState } from 'react';

import { useNavigate } from 'react-router-dom';

import { useChainStore } from '../popup';

import { ContractLogo } from '../../components/contract-logo';
import { contractStoreUtils, SubscribeContractItem } from '../../utils/storage';
import { List, Status } from 'tea-component';
import '../../iconsvg/index';
import { balanceOf } from '../../utils/utils';
import { FixedSizeList } from 'react-window';
import { CONTRACT_TYPE } from '../../config/contract';

const Row = (contractList, gasBalance) =>
  // eslint-disable-next-line react/display-name
  memo(({ index, style }: { index: number; style: CSSProperties }) => {
    const contract = contractList[index];
    const navigate = useNavigate();
    return (
      <div key={index} style={style}>
        <List.Item
          className={'flex-space-between contract-list-item'}
          onClick={() => {
            navigate(`/subscribe-contract/contract-detail`, {
              state: { ...contract, gasBalance },
            });
          }}
        >
          <div className="flex">
            <ContractLogo logoToken={contract.contractIcon} size={28} />
            <div className="txs-h3 contract-item-name">
              {' '}
              {contract.contractType === CONTRACT_TYPE.CMDFA
                ? contract.FTPerName
                : contract.remark || contract.contractName}
            </div>
          </div>
          <div className="contract-item-right">
            {contract.contractType === CONTRACT_TYPE.CMDFA && (
              <span className="text-normal contract-item-desc">
                {Number(contract.balance || 0).toFixed(contract.FTMinPrecision)}
              </span>
            )}
            {contract.contractType === CONTRACT_TYPE.GAS && (
              <span className="text-normal contract-item-desc">{gasBalance}</span>
            )}
            <svg className="tea-ml-2n" width="6px" height="12px" version="1.1" xmlns="http://www.w3.org/2000/svg">
              <polyline
                points="0,0 6,6 0,12"
                style={{
                  fill: 'transparent',
                  stroke: '#666',
                  strokeWidth: '1',
                }}
              />
            </svg>
          </div>
        </List.Item>
      </div>
    );
  });

export function ContractList({ gasBalance, ...props }: { gasBalance: number }) {
  // const location = useLocation();
  // const navigate = useNavigate();

  const [contractList, setContractList] = useState([]);
  const { selectedChain, currentAccount } = useChainStore();
  const accountId = currentAccount?.address;
  const chainId = selectedChain?.chainId;
  useEffect(() => {
    if (!chainId) return;
    contractStoreUtils.getSubscribe(chainId).then((contractList) => {
      setContractList(contractList);
      contractList.forEach((ele: SubscribeContractItem & { balance?: string }, index) => {
        const { contractType, contractName } = ele;
        if (!accountId) return;
        if (contractType === CONTRACT_TYPE.CMDFA) {
          // 先拿缓存
          contractStoreUtils.getContractBalance({ accountId, chainId, contractName }).then((value) => {
            contractList = Object.assign([], contractList);
            contractList.splice(index, 1, { ...ele, balance: value });

            setContractList(contractList);
          });
          // 再动态查询
          balanceOf({ contractName, account: currentAccount, chainId })
            .then((res) => {
              if (res !== ele.balance) {
                contractList = Object.assign([], contractList);
                contractList.splice(index, 1, { ...ele, balance: res });

                setContractList(contractList);
                contractStoreUtils.setContractBalance({
                  accountId,
                  chainId,
                  contractName,
                  balance: res,
                });
              }
            })
            .catch((err) => {
              console.error(err);
            });
        }
      });
    });
  }, [chainId, accountId]);

  return (
    <List {...props}>
      {!contractList.length ? (
        <Status icon={'blank'} size={'l'} title={'暂无数据'} className="cancel-bold" />
      ) : (
        <FixedSizeList
          height={245}
          itemCount={contractList.length}
          itemSize={57}
          width={'100%'}
          className={'txlogs-vtable'}
        >
          {Row(contractList, accountId ? gasBalance : '--')}
        </FixedSizeList>
      )}
    </List>
  );
}
