/*
 *
 *  Copyright (C) THL A29 Limited, a Tencent company. All rights reserved.
 *  SPDX-License-Identifier: Apache-2.0
 *
 */
import React, { useCallback, useEffect, useState, useRef, useMemo } from 'react';
import { useLocation, useNavigate } from 'react-router-dom';
import { Button, message } from 'tea-component';
import { ContractList } from './contract-list';
import { ConfirmModal } from '../../utils/common';
import { Account } from '../../utils/interface';
import chainStorageUtils, { contractStoreUtils } from '../../utils/storage';
import { useChainStore } from '../popup';
import { AccountSelect } from '../account/account-select';
import HeaderIcon from '../account/heaederIcon';
import { getNowSeconds, sendMessageToContentScript } from '../../utils/tools';
import { responseAccountInfo } from '../../utils/utils';
import { getBalanceGas } from '../../services/gas';
import { AccountCard } from './account-card';
import { CONTRACT_TYPE } from '../../config/contract';
import { DidInfo } from './did-info';
import { isSupportDidChain } from '../../services/did';

enum HomeTabs {
  CONTRACT,
  DID_VC,
}
const HOME_TAB_OPTIONS = [
  {
    title: '合约列表',
    value: HomeTabs.CONTRACT,
  },
];

const HOME_DID_TAB = {
  title: 'DID信息',
  value: HomeTabs.DID_VC,
};
export default function HomePage() {
  const navigate = useNavigate();
  const location = useLocation();
  //   const [contracts, setContracts] = useState([]);
  const [connected, setConnected] = useState(false);
  const [accountSelectVisible, setAccountSelectVisible] = useState(false);
  const { selectedChain, currentAccount, setCurrentAccount, currentTab } = useChainStore();
  const switchConfirmRef = useRef<typeof ConfirmModal>();
  const [gasBalance, setGasBalance] = useState(0);
  const [homeTabActive, setHomeTabActive] = useState(HomeTabs.CONTRACT);
  const openSelect = useCallback(() => {
    if (currentAccount) {
      setAccountSelectVisible(true);
    }
  }, [currentAccount]);

  const selectAccount = useCallback(
    async (ac: Account) => {
      const orgaccount = currentAccount;
      const account = await chainStorageUtils.setCurrentAccount(ac.address || '');
      setAccountSelectVisible(false);
      if (orgaccount?.authHosts?.includes(currentTab?.host || '') && !account.authHosts?.includes(currentTab?.host)) {
        setCurrentAccount(account);
        // @ts-ignore
        switchConfirmRef.current.show({
          confirm: async () => {
            const accounts = await chainStorageUtils.getCurrentChainAccounts();
            const newAccount = accounts.find((item) => item.address === ac.address);
            if (!newAccount.authHosts) {
              newAccount.authHosts = [];
            }
            newAccount.authHosts.push(currentTab?.host);
            await chainStorageUtils.setCurrentChainAccount(accounts);
            chainStorageUtils.setCurrentAccount(ac.address).then((res) => {
              setCurrentAccount(res);
              const { chainName, chainId } = selectedChain;
              sendMessageToContentScript({
                operation: 'changeConnectedAccounts',
                data: {
                  status: 'done',
                  timestamp: getNowSeconds(),
                  info: {
                    connectedAccounts: responseAccountInfo([res], currentTab?.host),
                    accounts: responseAccountInfo(accounts, currentTab?.host),
                    chain: { chainName, chainId },
                  },
                },
              });
            });
          },
        });
      } else {
        setCurrentAccount(account);
      }
    },
    [currentAccount],
  );
  const toSetting = useCallback(() => {
    if (connected) {
      navigate('/accounts/connect-setting');
    }
  }, [connected]);
  const toNewContract = useCallback(() => {
    if (currentAccount) {
      navigate('/subscribe-contract/new');
    }
  }, [currentAccount]);

  useEffect(() => {
    setConnected(Boolean(currentTab?.host && currentAccount?.authHosts?.includes(currentTab?.host)));
    // 获取gas selectedChain, currentAccount
    if (currentAccount && selectedChain.enableGas) {
      contractStoreUtils
        .getContractBalance({
          accountId: currentAccount.address,
          chainId: selectedChain.chainId,
          contractName: CONTRACT_TYPE.GAS,
        })
        .then((cacheBalance) => {
          setGasBalance(+cacheBalance || 0);
          getBalanceGas({ chainId: selectedChain.chainId, account: currentAccount }).then((result) => {
            if (result !== +cacheBalance) {
              setGasBalance(result || 0);
              contractStoreUtils.setContractBalance({
                accountId: currentAccount.address,
                chainId: selectedChain.chainId,
                contractName: CONTRACT_TYPE.GAS,
                balance: result,
              });
            }
          });
        });
    }
  }, [currentAccount]);
  useEffect(() => {
    const state = location.state as any;
    if (state?.alert) {
      message.error({
        content: state.alert,
        duration: 5000,
      });
    }
  }, []);

  const supporDid = useMemo(() => isSupportDidChain(selectedChain?.chainId), [selectedChain]);
  const homeTabOptions = useMemo(() => {
    const result = [...HOME_TAB_OPTIONS];
    if (supporDid) {
      result.push(HOME_DID_TAB);
    }
    return result;
  }, [selectedChain]);
  return (
    <>
      <div className="home-t">
        <div className={`home-status ${connected ? 'link' : ''}`} onClick={toSetting}>
          <div className="icon"></div>
          {connected ? currentTab?.host : '未连接'}
        </div>
        <div className="home-user">
          {currentAccount ? (
            <AccountCard
              account={currentAccount}
              enableGas={!!selectedChain.enableGas}
              gasBalance={gasBalance}
              onOpenSelect={openSelect}
            />
          ) : (
            <>
              <HeaderIcon
                color="#BF760A&#F4EA2A"
                onClick={() =>
                  navigate('/accounts/new', {
                    state: {
                      chain: selectedChain,
                      initial: false,
                    },
                  })
                }
                classN="home-header"
                width={70}
                height={70}
              />
              <div className="home-account">
                暂无数据，请先
                <span
                  onClick={() =>
                    navigate('/accounts/new', {
                      state: {
                        chain: selectedChain,
                        initial: false,
                      },
                    })
                  }
                >
                  添加链账户
                </span>
              </div>
            </>
          )}
        </div>
      </div>
      <div className="contract-ls-b">
        <div className="contract-ls-t">
          {homeTabOptions.map(({ title, value }) => (
            <div
              key={value}
              onClick={() => {
                setHomeTabActive(value);
              }}
              className={homeTabActive === value ? 'home-tab-active mr-5n' : 'mr-5n'}
            >
              {title}
            </div>
          ))}
        </div>
      </div>
      {homeTabActive === HomeTabs.CONTRACT ? (
        <>
          <div className="contract-ls-">
            <ContractList gasBalance={gasBalance} />
          </div>
          <footer>
            {/* <ProductGuideLink /> */}
            {currentAccount && (
              <Button type={'link'} className={`flex-items-center`} onClick={toNewContract}>
                <img width={20} height={20} src="../../../img/icon-add.png" alt="" />
                <span className="ml-2n">订阅新合约</span>
              </Button>
            )}
          </footer>
        </>
      ) : null}
      {supporDid && (
        <div style={{ display: homeTabActive === HomeTabs.DID_VC ? 'block' : 'none' }}>
          <DidInfo />
        </div>
      )}
      {/* {homeTabActive === HomeTabs.DID_VC ? <DidInfo /> : null} */}

      <AccountSelect
        onSelect={(ac) => {
          selectAccount(ac);
        }}
        visible={accountSelectVisible}
        onClose={() => setAccountSelectVisible(false)}
      />
      <ConfirmModal
        title={'切换链账户授权'}
        confirmBtnText={'确定授权'}
        cancelBtnText="暂不授权"
        ref={switchConfirmRef}
      >
        {`您准备切换的链账户“${currentAccount?.name}”尚未授权给“${currentTab?.host}”，请确定是否要授权。如果不授权的话，将临时中断和该Dapp的连接。`}
      </ConfirmModal>
    </>
  );
}
