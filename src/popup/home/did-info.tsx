import React, { CSSProperties, memo, useEffect, useState } from 'react';

import { useNavigate } from 'react-router-dom';

import { useChainStore } from '../popup';

import chainStorageUtils, { DidItem, VcItem } from '../../utils/storage';
import { Copy, List, Status } from 'tea-component';
import '../../iconsvg/index';
import { FixedSizeList } from 'react-window';
import { getAccountDid, getDidVcList } from '../../services/did';
import { HeaderCard } from '../../components/header-card';
import SvgIcon from '../../components/svg-icon';

const Row = (vcList: VcItem[]) =>
  // eslint-disable-next-line react/display-name
  memo(({ index, style }: { index: number; style: CSSProperties }) => {
    const vc = vcList[index];
    const navigate = useNavigate();
    return (
      <div key={index} style={style}>
        <List.Item
          className={'flex-space-between did-list-item'}
          onClick={() => {
            navigate(`/did/vc-detail`, {
              state: { vcData: vc },
            });
          }}
        >
          <HeaderCard
            borderNone
            icon={<SvgIcon width={40} height={40} name="identityCert" />}
            content={<p className="overflow-ellipsis font-16 font-bold">{vc.credentialSubject.certificateName}</p>}
          />
        </List.Item>
      </div>
    );
  });

export function DidInfo({ ...props }) {
  // const location = useLocation();
  const navigate = useNavigate();

  const [vcList, setVcList] = useState<VcItem[]>([]);
  const [did, setDid] = useState('');
  const [didDocument, setDidDocument] = useState<DidItem>(null);
  const [isLoading, setIsLoading] = useState(true);
  const { selectedChain, currentAccount } = useChainStore();
  const accountId = currentAccount?.address;
  const chainId = selectedChain?.chainId;
  useEffect(() => {
    if (!chainId || !accountId) return setIsLoading(false);
    (async () => {
      const doc = await chainStorageUtils.getDidDocument({ chainId, accountAddress: accountId });
      let storeVcList = [];
      const currentDid = doc?.id;
      // 本地数据更新
      if (currentDid) {
        storeVcList = await chainStorageUtils.getDidVCList({ chainId, did: doc.id });
        setDid(currentDid);
        setDidDocument(doc);
        setVcList(storeVcList);
        setIsLoading(false);
      }
      // 链数据更新
      const chainDidDoc = await getAccountDid({ account: currentAccount, chainId });

      if (!chainDidDoc) {
        setDid(null);
        setDidDocument(null);
        setVcList([]);
        setIsLoading(false);
        if (currentDid) {
          // 如果链上did移除，本地有就清除
          chainStorageUtils.clearAccountDidDocument({ chainId, accountAddress: accountId });
        }
        return;
      }
      const updateVcList = await getDidVcList({
        account: currentAccount,
        did: chainDidDoc.id,
        chainId,
        verificationMethod: chainDidDoc.accounts.find((ele) => ele.address === currentAccount.address)?.id,
      });
      setIsLoading(false);
      setDid(chainDidDoc.id);
      setDidDocument(chainDidDoc);
      setVcList(updateVcList);
    })();
  }, [chainId, accountId]);
  if (isLoading) {
    return <Status icon={'loading'} size={'l'} title={'数据加载中'} className="cancel-bold" />;
  }

  return (
    <List {...props}>
      {!did ? (
        <Status icon={'blank'} size={'l'} title={'暂无数据'} className="cancel-bold" />
      ) : (
        <>
          <List.Item>
            {/* <div className="flex">
             
              <div className="txs-h3 contract-item-name">{did}</div>
            </div> */}
            {/* <div className="tea-mr-2n">
              <Copy text={did} onCopy={() => false} />
            </div> */}
            <HeaderCard
              className="home-did-item"
              icon={
                <span
                  onClick={() => {
                    navigate(`/did/did-detail`, {
                      state: {
                        didDocument,
                      },
                    });
                  }}
                >
                  <SvgIcon width={40} height={40} name="did" />
                </span>
              }
              content={
                <p>
                  <span
                    onClick={() => {
                      navigate(`/did/did-detail`, {
                        state: {
                          didDocument,
                        },
                      });
                    }}
                  >
                    {did}{' '}
                  </span>{' '}
                  <Copy text={did} onCopy={() => false} />
                </p>
              }
            />
          </List.Item>
          {!vcList.length ? (
            <Status icon={'blank'} title={'暂无证书'} className="cancel-bold" />
          ) : (
            <FixedSizeList
              height={220}
              itemCount={vcList.length}
              itemSize={70}
              width={'100%'}
              className={'txlogs-vtable'}
            >
              {Row(vcList)}
            </FixedSizeList>
          )}
        </>
      )}
    </List>
  );
}
