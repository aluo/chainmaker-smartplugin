import React from 'react';
import { Text, Copy } from 'tea-component';
import { Account } from '../../utils/interface';
import HeaderIcon from '../account/heaederIcon';

import './home.less';
import { showHeadandTail } from '../../utils/utils';
interface AccountCardProps {
  enableGas: boolean;
  account: Account;
  onOpenSelect: () => void;
  gasBalance: number;
}

export const AccountCard = (props: AccountCardProps) =>
  props.enableGas ? <GasCard {...props} /> : <NormalCard {...props} />;

const NormalCard = ({ account, onOpenSelect }: AccountCardProps) => (
  <>
    <HeaderIcon color={account?.color} onClick={onOpenSelect} classN="home-header" width={70} height={70} />
    <div className="home-account">{account.name}</div>
    <div className="home-account-code">
      <Text copyable>{account.address}</Text>
    </div>
  </>
);

const GasCard = ({ account, gasBalance, onOpenSelect }: AccountCardProps) => (
  <div className="account-gas-card">
    <div className="gas-account">
      <HeaderIcon color={account?.color} width={36} height={36} onClick={onOpenSelect} />
      <div className="gas-account-code">
        <Text onClick={onOpenSelect}>{showHeadandTail(account.address, 12)}</Text>{' '}
        <Copy text={account.address} onCopy={() => false} />
      </div>
    </div>
    <div className="gas-balance">
      <p className="">{gasBalance || 0} </p>
      <span className="gas">GAS</span>
    </div>
  </div>
);
