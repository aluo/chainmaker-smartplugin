/*
 *
 *  Copyright (C) THL A29 Limited, a Tencent company. All rights reserved.
 *  SPDX-License-Identifier: Apache-2.0
 *
 */

import React, { CSSProperties, memo, useCallback, useEffect, useState, useRef } from 'react';
import { useTxLogs } from '../utils/hooks';
import { Icon, Text, Status } from 'tea-component';
import { useChainStore } from './popup';
import { DetailPage, ReadonlyFormItem } from '../utils/common';
import { formatDate, getSyncLogResult } from '../utils/utils';
import { FixedSizeList } from 'react-window';
import { TxLog } from '../utils/interface';
import chainStorageUtils from '../utils/storage';
import { getBrowserTransactionLink } from '../config/chain';

const Row = (logs: TxLog[], refreshLog: (log: TxLog & { index: number }) => void, browserLink?: string) =>
  // eslint-disable-next-line react/display-name
  memo(({ index, style }: { index: number; style: CSSProperties }) => {
    const item = logs[index] as TxLog & { isLoading: boolean };
    const pendingStatus = (
      <Text
        theme={'warning'}
        onClick={() => {
          refreshLog({ ...item, index });
        }}
      >
        上链中 {item.isLoading ? <Icon type="loading" /> : <Icon type="refresh" />}
      </Text>
    );
    return (
      <div key={index} style={style}>
        <div className={'txlogs-item'}>
          <div className={'flex meta-layout'}>
            <div className={'status'}>
              {item.status === 'pending' ? (
                pendingStatus
              ) : (
                <Text theme={item.code === 0 ? 'success' : 'danger'}>{item.code === 0 ? '上链成功' : '上链失败'}</Text>
              )}
            </div>
            <div className={'datetime'}>{formatDate(new Date(item.timestamp * 1000))}</div>
          </div>
          <div className={'flex meta-layout'}>
            <div>{item.accountName}</div>
            <div></div>
          </div>
          <ReadonlyFormItem
            href={browserLink && getBrowserTransactionLink({ browserLink, txId: item.txId, chainId: item.chainId })}
            text={item.txId}
            theme={'transparent'}
          />
        </div>
      </div>
    );
  });

function TxLogsPage() {
  const chainSelected = useChainStore((state) => state.selectedChain);
  const [acconts, setAccounts] = useState([]);
  const chainId = chainSelected?.chainId;
  const browserLink = chainSelected?.browserLink;
  const isUpdate = useRef(false);
  const { logs, setLogs } = useTxLogs(chainId);
  // 更新单个日志
  const updateTxLog = useCallback(
    ({ index, account }) => {
      getSyncLogResult({ chainId, account, log: logs[index] })
        .then((result) => {
          const newEle = Object.assign({}, logs[index], { code: result.code, status: 'done', isLoading: false });
          // 清除冗余数据
          delete newEle.contractName;
          delete newEle.params;
          delete newEle.method;

          const newLogs = Object.assign([], logs);
          newLogs.splice(index, 1, newEle);
          setLogs(newLogs);
        })
        .catch(() => {
          // 更新失败结束loading
          const newEle = Object.assign({}, logs[index], { isLoading: false });
          const newLogs = Object.assign([], logs);
          newLogs.splice(index, 1, newEle);
          setLogs(newLogs);
        });
    },
    [logs, setLogs],
  );

  // 获取所有账号
  useEffect(() => {
    chainStorageUtils.getChainAccounts(chainSelected.chainId).then((res) => {
      setAccounts(res);
    });
  }, []);
  // // 刷新单个日志
  const refreshLog = useCallback(
    ({ txId, accountId, index }: TxLog & { index: number }) => {
      // acconts
      let meatched = false;
      acconts.forEach((ele) => {
        if (ele.address === accountId) {
          meatched = true;
          // start loading
          const newEle = Object.assign({}, logs[index], { isLoading: true });
          const newLogs = Object.assign([], logs);
          newLogs.splice(index, 1, newEle);
          setLogs(newLogs);
          // start loading  end
          updateTxLog({ account: ele, index, txId });
        }
      });
      if (!meatched) {
        console.warn('未匹配到上链日志发起账户');
      }
    },
    [logs, setLogs],
  );
  // // 初始化时,更新前十条pedding日志状态
  useEffect(() => {
    if (!isUpdate.current && acconts.length) {
      let num = 0;
      logs.forEach((log, index) => {
        if (log.status === 'pending' && num < 10) {
          let meatched = false;
          acconts.forEach((ele) => {
            if (ele.address === log.accountId) {
              meatched = true;
              // 通过日志
              num += 1;
              updateTxLog({ account: ele, index, txId: log.txId });
            }
          });
          if (!meatched) {
            console.warn('未匹配到上链日志发起账户', log);
          }
        }
      });
      isUpdate.current = true;
    }
  }, [logs, acconts]);

  return (
    <>
      <DetailPage title={'上链记录'} backUrl={'/'}>
        {logs.length === 0 ? (
          <Status icon={'blank'} size={'l'} title={'暂无上链记录'} className="cancel-bold" />
        ) : (
          <FixedSizeList height={430} itemCount={logs.length} itemSize={132} width={'100%'} className={'txlogs-vtable'}>
            {Row(logs, refreshLog, browserLink)}
          </FixedSizeList>
        )}
      </DetailPage>
    </>
  );
}

export default TxLogsPage;
