/*
 *
 *  Copyright (C) THL A29 Limited, a Tencent company. All rights reserved.
 *  SPDX-License-Identifier: Apache-2.0
 *
 */

import React, { useEffect, useState } from 'react';
import { useLocation, useNavigate } from 'react-router-dom';
import { DetailPage, CancelSubscribe } from '../../../utils/common';
import { Status, Text } from 'tea-component';

import { queryNFTList } from '../../../utils/utils';
import { ContractNFTItem, contractStoreUtils, SubscribeContractItem } from '../../../utils/storage';
import { useChainStore } from '../../popup';
import { NFTImage } from '../../../components/nft-image';

const Row = ({
  item,
  contractInfo,
  browserLink,
}: {
  item: ContractNFTItem;
  contractInfo: SubscribeContractItem;
  browserLink: string;
}) => {
  const navigate = useNavigate();
  return (
    <div
      className="tea-mb-5n nft-item-wrap"
      onClick={() =>
        navigate('/subscribe-contract/transaction-detail', {
          state: {
            metadata: item,
            contractInfo,
            browserLink,
          },
        })
      }
    >
      <div className={'flex meta-layout'}>
        <NFTImage url={item.image} />
      </div>
      <div className={'flex meta-layout tea-mt-2n'}>
        <Text theme="strong">
          <b>{item.name}</b>
        </Text>
      </div>
    </div>
  );
};

export function CMNFAContractPage() {
  const location = useLocation();
  const contractInfo = location.state as SubscribeContractItem;
  const [NFTList, setNFTList] = useState<ContractNFTItem[]>([]);
  const [loading, setLoading] = useState(true);
  const { contractName } = contractInfo;
  const { selectedChain, currentAccount } = useChainStore();
  const accountId = currentAccount?.address;
  const chainId = selectedChain?.chainId;
  const browserLink = selectedChain?.browserLink;

  useEffect(() => {
    // 查询tokens
    if (!accountId) return;
    // 添加缓存数据
    contractStoreUtils.getContractNFT({ chainId, accountId, contractName }).then((NFTList) => setNFTList(NFTList));
    // 查询更新数据
    queryNFTList({
      chainId: selectedChain.chainId,
      account: currentAccount,
      contractName,
    })
      .then((NFTList) => {
        setLoading(false);
        setNFTList(NFTList);
      })
      .catch(() => {
        setLoading(false);
      });
  }, [loading]);
  return (
    <DetailPage title={'合约详情'} backUrl={'/'}>
      {!NFTList || NFTList.length === 0 ? (
        <Status
          icon={loading ? 'loading' : 'blank'}
          size={'l'}
          className="cancel-bold"
          title={loading ? '加载中...' : '暂无相关记录'}
        />
      ) : (
        <div className="nft-list">
          {NFTList.map((item, index) => (
            <Row key={index} item={item} contractInfo={contractInfo} browserLink={browserLink} />
          ))}
        </div>
      )}
      <CancelSubscribe chainId={chainId} contractName={contractName}></CancelSubscribe>
    </DetailPage>
  );
}
