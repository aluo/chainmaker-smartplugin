/*
 *
 *  Copyright (C) THL A29 Limited, a Tencent company. All rights reserved.
 *  SPDX-License-Identifier: Apache-2.0
 *
 */

import React, { CSSProperties, memo, useEffect, useState } from 'react';
import { useLocation, useNavigate } from 'react-router-dom';
import { DetailPage, CancelSubscribe } from '../../../utils/common';
import { Status } from 'tea-component';

import { FixedSizeList } from 'react-window';
import { balanceOf } from '../../../utils/utils';
import { contractStoreUtils, SubscribeContractItem } from '../../../utils/storage';
import { useChainStore } from '../../popup';
import { ContractLogo } from '../../../components/contract-logo';
import { TxsItem } from '../../../components/txs-item';

const Row = (list, contract, browserLink) =>
  // eslint-disable-next-line react/display-name
  memo(({ index, style }: { index: number; style: CSSProperties }) => {
    const item = list[index];
    const navigate = useNavigate();
    return (
      <div key={index} style={style}>
        <TxsItem
          {...item}
          {...contract}
          copyable={false}
          onClick={() =>
            navigate('/subscribe-contract/transaction-detail', {
              state: {
                txInfo: item,
                contractInfo: contract,
                browserLink,
              },
            })
          }
        />
      </div>
    );
  });

export function CMDFAContractPage() {
  const location = useLocation();
  const contractInfo = location.state as SubscribeContractItem & { balance: string };
  const [txList, setTxsList] = useState([]);
  const { contractName, FTPerName, FTMinPrecision, contractIcon, balance } = contractInfo;
  const [sum, setSum] = useState(balance);
  const { selectedChain, currentAccount } = useChainStore();
  const accountId = currentAccount?.address;
  const chainId = selectedChain?.chainId;
  const browserLink = selectedChain?.browserLink;

  useEffect(() => {
    // 查询合约余额
    if (!accountId) return;
    balanceOf({ contractName, account: currentAccount, chainId }).then((res: string) => {
      if (balance !== res) {
        setSum(res);
        contractStoreUtils.setContractBalance({ accountId, contractName, chainId, balance: res });
      }
    });
    // 查询转账交易
    contractStoreUtils
      .getContractTxs({
        chainId,
        contractName,
        accountId,
      })
      .then((res) => {
        setTxsList(res);
      });
  }, [accountId, contractName]);

  return (
    <DetailPage title={'合约详情'} className="free-width" backUrl={'/'}>
      <div className="contract-header">
        <div className="contract-header-logo">
          <ContractLogo logoToken={contractIcon} size={40} />
        </div>
        <div className="contract-header-title">
          {Number(sum || 0).toFixed(FTMinPrecision)} {FTPerName}
        </div>
      </div>
      <div className="txs-h2 tea-mt-4n">交易记录</div>
      <div className="txs-list">
        {txList.length === 0 ? (
          <Status icon={'blank'} size={'l'} className="cancel-bold" title={'暂无转账记录'} />
        ) : (
          <FixedSizeList
            height={430}
            itemCount={txList.length}
            itemSize={77}
            width={'100%'}
            className={'txlogs-vtable'}
          >
            {Row(txList, contractInfo, browserLink)}
          </FixedSizeList>
        )}
      </div>
      <CancelSubscribe chainId={chainId} contractName={contractName}></CancelSubscribe>
    </DetailPage>
  );
}
