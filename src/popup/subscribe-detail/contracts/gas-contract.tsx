/*
 *
 *  Copyright (C) THL A29 Limited, a Tencent company. All rights reserved.
 *  SPDX-License-Identifier: Apache-2.0
 *
 */

import React, { CSSProperties, memo, useEffect, useState } from 'react';
import { useLocation } from 'react-router-dom';
import { DetailPage } from '../../../utils/common';
import { Status } from 'tea-component';

import { FixedSizeList } from 'react-window';
import { contractStoreUtils, SubscribeContractItem } from '../../../utils/storage';
import { useChainStore } from '../../popup';
import { ContractLogo } from '../../../components/contract-logo';
import { TxsItem } from '../../../components/txs-item';
import { getBalanceGas } from '../../../services/gas';
import { getBrowserTransactionLink } from '../../../config/chain';

const Row = ({ list, contract, browserLink, chainId }) =>
  // eslint-disable-next-line react/display-name
  memo(({ index, style }: { index: number; style: CSSProperties }) => {
    const item = list[index];
    return (
      <div key={index} style={style}>
        <TxsItem
          {...item}
          {...contract}
          copyable={false}
          href={browserLink && getBrowserTransactionLink({ browserLink, txId: item.txId, chainId })}
        />
      </div>
    );
  });

export function GASContractPage() {
  const location = useLocation();
  const contractInfo = location.state as SubscribeContractItem & { gasBalance: string };
  const [txList, setTxsList] = useState([]);
  const { contractName, contractIcon, gasBalance: balance } = contractInfo;
  const [gasBalance, setGasBalance] = useState(balance);
  const { selectedChain, currentAccount } = useChainStore();
  const accountId = currentAccount?.address;
  const chainId = selectedChain?.chainId;
  const browserLink = selectedChain?.browserLink;

  useEffect(() => {
    // 查询合约余额
    if (!accountId) return;
    getBalanceGas({ chainId, account: currentAccount }).then((result) => {
      setGasBalance(`${result}`);
    });

    // 查询转账交易
    contractStoreUtils
      .getContractTxs({
        chainId,
        contractName,
        accountId,
      })
      .then((res) => {
        setTxsList(res);
      });
  }, [accountId, contractName]);

  return (
    <DetailPage title={'合约详情'} className="free-width" backUrl={'/'}>
      <div className="contract-header">
        <div className="contract-header-logo">
          <ContractLogo logoToken={contractIcon} size={40} />
        </div>
        <div className="contract-header-title">{gasBalance || 0} GAS</div>
      </div>
      <div className="txs-h2 tea-mt-4n">使用记录</div>
      <div className="txs-list">
        {txList.length === 0 ? (
          <Status icon={'blank'} size={'l'} className="cancel-bold" title={'暂无使用记录'} />
        ) : (
          <FixedSizeList
            height={430}
            itemCount={txList.length}
            itemSize={77}
            width={'100%'}
            className={'txlogs-vtable'}
          >
            {Row({ list: txList, contract: contractInfo, browserLink, chainId })}
          </FixedSizeList>
        )}
      </div>
    </DetailPage>
  );
}
