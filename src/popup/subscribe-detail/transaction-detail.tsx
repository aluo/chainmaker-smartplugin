/*
 *
 *  Copyright (C) THL A29 Limited, a Tencent company. All rights reserved.
 *  SPDX-License-Identifier: Apache-2.0
 *
 */

import React from 'react';
import { useLocation } from 'react-router-dom';
import { CONTRACT_TYPE } from '../../config/contract';
import { SubscribeContractItem } from '../../utils/storage';

import { FtDetailPage } from './transactions/ft-detail';
import { NftDetailPage } from './transactions/nft-detail';
import { CmeviDetailPage } from './transactions/cmevi-detail';

export function TransactionDetailPage(props) {
  const location = useLocation();
  const { contractInfo } = location.state as { contractInfo: SubscribeContractItem; txInfo: any };
  const { contractType } = contractInfo;
  switch (contractType) {
    case CONTRACT_TYPE.CMDFA:
      return <FtDetailPage {...props} />;
    case CONTRACT_TYPE.CMNFA:
      return <NftDetailPage {...props} />;
    case CONTRACT_TYPE.CMEVI:
      return <CmeviDetailPage {...props} />;
    default:
      return null;
  }
}
