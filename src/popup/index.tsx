/*
 *
 *  Copyright (C) THL A29 Limited, a Tencent company. All rights reserved.
 *  SPDX-License-Identifier: Apache-2.0
 *
 */

import * as React from 'react';
import * as ReactDOM from 'react-dom';
import Popup from './popup';

ReactDOM.render(<Popup/>, document.getElementById('popup'));
