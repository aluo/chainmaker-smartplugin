/*
 *
 *  Copyright (C) THL A29 Limited, a Tencent company. All rights reserved.
 *  SPDX-License-Identifier: Apache-2.0
 *
 */

import React, { useCallback, useEffect, useState, useRef } from 'react';
import { Button, Form, Input, message, Select, Switch } from 'tea-component';
import { Controller, useForm, useWatch } from 'react-hook-form';
import formUtils, { Validator } from '../utils/form-utils';
import { UploadFile } from '../utils/upload-file';
import { useNavigate } from 'react-router-dom';
import chainStorageUtils from '../utils/storage';
import { Account, AccountForm, Chain, ChainForm, PluginSetting } from '../utils/interface';
import { DetailPage, ConfirmModal } from '../utils/common';
import { getAddressFromKeyPemFile } from '../../web-sdk/glue';
import {
  accountModeOptions,
  getChainConfig,
  pick,
  readNameFromCert,
  uploadCerts,
  trimFileNameSuffix,
  getChainVersion,
  protocolOptions,
  isSupportHTTP,
} from '../utils/utils';
import { useChainStore } from './popup';
import GlossaryGuide from '../utils/glossary-guide';
import { DEFAULT_HOSTNAME } from '../config/chain';
import { initChainSubscribe } from '../services/chain';
import { BEACON_EVENTS, beacon } from '../beacon';

export async function createCrtName(values: AccountForm, mode: Chain['accountMode']) {
  if (mode === 'permissionedWithCert') {
    return await readNameFromCert(values.userSignCrtFile);
  }
  return null;
}
export const protocolExtraMap = {
  GRPC: (
    <ul className="tips-list">
      <li>
        默认通信方式，需要配合
        <a
          target="_blank"
          href="https://docs.chainmaker.org.cn/dev/%E9%95%BF%E5%AE%89%E9%93%BEWeb3%E6%8F%92%E4%BB%B6.html#id29"
          rel="noreferrer"
        >
          代理服务
        </a>
        使用
      </li>
    </ul>
  ),
  HTTP: (
    <ul>
      <li>
        chainmaker2.3.0及以上支持，需通过
        <a
          target="_blank"
          href="https://docs.chainmaker.org.cn/dev/%E9%95%BF%E5%AE%89%E9%93%BEWeb3%E6%8F%92%E4%BB%B6.html#id26"
          rel="noreferrer"
        >
          链配置
        </a>
        开启
      </li>
    </ul>
  ),
};

function ChainNewPage() {
  const {
    control,
    formState: { isValidating, isSubmitted, isValid },
    getValues,
    setValue,
    trigger,
  } = useForm({
    mode: 'onBlur',
    defaultValues: {
      tlsEnable: true,
      protocol: 'GRPC',
      hostName: DEFAULT_HOSTNAME,
      accountMode: 'permissionedWithCert',
    } as any,
  });
  const navigate = useNavigate();
  const { chains, setChains, setSelectedChain, setCurrentAccount } = useChainStore((state) => state);

  // const chains = useChainStore(state => state.chains);
  // const setChains = useChainStore(state => state.setChains);
  // const setSelectedChain = useChainStore(state => state.setSelectedChain);
  const initialized = useChainStore((state) => state.initialized);
  const setInitialized = useChainStore((state) => state.setInitialized);
  const [pluginSetting, setPluginSetting] = useState<PluginSetting>();
  const [address, setAddress] = useState('');

  const tlsEnable: ChainForm['tlsEnable'] = useWatch({ name: 'tlsEnable', control });
  const protocol: ChainForm['protocol'] = useWatch({ name: 'protocol', control });
  const accountMode: ChainForm['accountMode'] = useWatch({ name: 'accountMode', control });

  const handleCancel = useCallback(() => {
    navigate('/chains');
  }, []);

  const [loading, setLoading] = useState(false);
  const httpFailRef = useRef<typeof ConfirmModal>();
  const chainFailRef = useRef<typeof ConfirmModal>();

  const handleClick = useCallback(async () => {
    setLoading(true);
    beacon.onUserAction(BEACON_EVENTS.BLOCKCHAIN_ADD_CONFIRM);
    const values = getValues() as ChainForm & AccountForm;
    const [userSignKeyFile, userSignCrtFile, userPublicKeyFile, userTLSKeyFilePath, userTLSCrtFilePath] =
      await chainStorageUtils.uploadFiles([
        values.userSignKeyFile,
        values.userSignCrtFile,
        values.userPublicKeyFile,
        values.userTLSKeyFile,
        values.userTLSCrtFile,
      ]);

    const chain: Chain = {
      ...pick(values, [
        'chainId',
        'chainName',
        'nodeIp',
        'tlsEnable',
        'accountMode',
        'hostName',
        'protocol',
        'hostName',
        'browserLink',
      ]),
      userTLSKeyFile: null,
      userTLSCrtFile: null,
      nodeTLSCrtFile: null,
      version: null,
      httpSupport: isSupportHTTP(values),
    };

    try {
      if (chain.httpSupport) {
        try {
          await getChainVersion(`http${chain.tlsEnable ? 's' : ''}://${chain.nodeIp}`);
        } catch (e) {
          console.error('getChainVersion error', e);
          if (e.code === 0 && values.tlsEnable) {
            // @ts-ignore
            httpFailRef.current.show();
          } else {
            // @ts-ignore
            chainFailRef.current.show();
          }
          setLoading(false);
          return;
        }
      } else if (chain.tlsEnable) {
        const { crtFileName, keyFileName } = await uploadCerts(values.userTLSKeyFile, values.userTLSCrtFile);
        chain.userTLSCrtFile = crtFileName;
        chain.userTLSKeyFile = keyFileName;
        // 本地存储tls字段
        chain.userTLSKeyFilePath = userTLSKeyFilePath;
        chain.userTLSCrtFilePath = userTLSCrtFilePath;
        chain.proxyURL = pluginSetting.proxyHostnameTls;
      }
      const account: Account =
        values.accountMode === 'public'
          ? {
              userSignKeyFile,
              userPublicKeyFile,
              name: trimFileNameSuffix(values.userSignKeyFile.name),
              crtName: await createCrtName(values, values.accountMode),
              address,
            }
          : {
              userSignKeyFile,
              userSignCrtFile,
              crtName: await createCrtName(values, values.accountMode),
              // 默认为证书文件名
              name: trimFileNameSuffix(values.userSignCrtFile.name),
              orgId: values.orgId,
              address,
            };

      const chainConfig = await getChainConfig(chain, {
        userSignKeyFile: values.userSignKeyFile,
        userSignCrtFile: values.userSignCrtFile,
        userPublicKeyFile: values.userPublicKeyFile,
        orgId: account.orgId,
      });

      chain.version = chainConfig.version;
      chain.enableGas = chainConfig.accountConfig?.enableGas;
      const [chains] = await Promise.all([
        chainStorageUtils.addChain(chain),
        chainStorageUtils.addChainAccount(chain.chainId, account),
      ]);
      setChains(chains as Chain[]);
      await initChainSubscribe(chain);
      // 自动选链
      if (chains.length === 1) {
        setSelectedChain(chains[0]);
        await chainStorageUtils.setSelectedChain(chains[0]);
        const account = await chainStorageUtils.getCurrentAccount();
        setCurrentAccount(account);
      }
      setLoading(false);
      if (initialized) {
        navigate('/chains');
      } else {
        setInitialized(true);
        navigate('/');
      }
    } catch (e) {
      console.error(e);
      setLoading(false);
      message.error({
        content: `添加区块链网络失败\n${e.message || e}`,
        duration: 5000,
      });
    }
  }, [initialized, pluginSetting, address]);

  const updateAddressByPuKeyFile = useCallback(async (file) => {
    const addr = await getAddressFromKeyPemFile(file);
    setAddress(addr);
  }, []);
  useEffect(() => {
    chainStorageUtils.getSetting().then((res) => {
      setPluginSetting(res);
    });
  }, []);

  useEffect(() => {
    trigger(['userTLSKeyFile', 'userTLSCrtFile', 'hostName']);
  }, [tlsEnable]);

  useEffect(() => {
    accountMode === 'public' && setValue('tlsEnable', false);
  }, [accountMode]);

  return (
    <>
      <DetailPage title={'添加区块链网络'} backUrl={'/chains'}>
        <Form layout={'vertical'}>
          <Controller
            control={control}
            rules={{
              validate: (value) => {
                const validateRes = Validator.validateChainId(value);
                if (validateRes) {
                  return validateRes;
                }
                if (chains.some((item) => item.chainId === value)) {
                  return '已添加过该链';
                }
              },
            }}
            name="chainId"
            render={({ field, fieldState }) => (
              <Form.Item
                status={formUtils.getStatus({
                  fieldState,
                  isValidating,
                  isSubmitted,
                })}
                label={<GlossaryGuide title={'区块链ID'} />}
                message={fieldState.error?.message}
              >
                <Input size={'full'} {...field} />
              </Form.Item>
            )}
          />
          <Controller
            control={control}
            rules={{
              required: '请输入',
            }}
            name="chainName"
            render={({ field, fieldState }) => (
              <Form.Item
                status={formUtils.getStatus({
                  fieldState,
                  isValidating,
                  isSubmitted,
                })}
                message={fieldState.error?.message}
                label={<GlossaryGuide title={'区块链网络名称'} />}
              >
                <Input size={'full'} {...field} />
              </Form.Item>
            )}
          />
          <Controller
            control={control}
            rules={{
              validate: Validator.validateNodeIP,
            }}
            name="nodeIp"
            render={({ field, fieldState }) => (
              <Form.Item
                status={formUtils.getStatus({
                  fieldState,
                  isValidating,
                  isSubmitted,
                })}
                message={fieldState.error?.message}
                label={<GlossaryGuide title={'节点RPC服务地址'} />}
              >
                <Input size={'full'} placeholder={'127.0.0.1:8080'} {...field} />
              </Form.Item>
            )}
          />
          <Controller
            control={control}
            name="protocol"
            render={({ field }) => (
              <Form.Item label={<GlossaryGuide title={'网络通讯方式'} />} extra={protocolExtraMap[protocol]}>
                <Select
                  size={'full'}
                  matchButtonWidth
                  appearance="button"
                  options={protocolOptions}
                  placeholder="网络通讯方式"
                  {...field}
                />
              </Form.Item>
            )}
          />
          {/* <Controller
          control={control}
          name="protocol"
          render={({ field }) => (
            <Form.Item
              label="选择通讯协议"
              extra={protocolExtraMap[protocol]}
              >
                <RadioGroup {
                  ...field
                }>
                  <Radio name="grpc">grpc</Radio>
                  <Radio name="http">http</Radio>
                  <Radio name="https">https</Radio>
              </RadioGroup>
             
            </Form.Item>
          )}
        /> */}
          <Controller
            control={control}
            rules={{
              required: '请输入',
            }}
            name="accountMode"
            render={({ field, fieldState }) => (
              <Form.Item
                status={formUtils.getStatus({
                  fieldState,
                  isValidating,
                  isSubmitted,
                })}
                message={fieldState.error?.message}
                label="账户模式"
              >
                <Select
                  size={'full'}
                  matchButtonWidth
                  appearance="button"
                  options={accountModeOptions}
                  placeholder="请选择身份模式"
                  {...field}
                />
              </Form.Item>
            )}
          />{' '}
          {accountMode === 'permissionedWithCert' && (
            <>
              <Controller
                control={control}
                rules={{
                  validate: Validator.validateOrgId,
                }}
                name="orgId"
                render={({ field, fieldState }) => (
                  <Form.Item
                    status={formUtils.getStatus({
                      fieldState,
                      isValidating,
                      isSubmitted,
                    })}
                    message={fieldState.error?.message}
                    label={<GlossaryGuide title={'用户所在组织ID'} />}
                  >
                    <Input size={'full'} {...field} />
                  </Form.Item>
                )}
              />
              <Controller
                control={control}
                rules={{
                  required: '请输入',
                }}
                name="userSignCrtFile"
                render={({ field, fieldState }) => (
                  <Form.Item
                    status={formUtils.getStatus({
                      fieldState,
                      isValidating,
                      isSubmitted,
                    })}
                    label="用户签名证书"
                  >
                    <UploadFile
                      {...field}
                      accept=".crt"
                      onSuccess={(key: File | null) => {
                        setValue(field.name, key, {
                          shouldValidate: true,
                        });
                      }}
                    />
                  </Form.Item>
                )}
              />
            </>
          )}
          <Controller
            control={control}
            rules={{
              required: '请输入',
            }}
            name="userSignKeyFile"
            render={({ field, fieldState }) => (
              <Form.Item
                status={formUtils.getStatus({
                  fieldState,
                  isValidating,
                  isSubmitted,
                })}
                label="用户签名私钥"
              >
                <UploadFile
                  {...field}
                  accept=".key"
                  onSuccess={(key: File | null) => {
                    setValue(field.name, key, {
                      shouldValidate: true,
                    });
                    updateAddressByPuKeyFile(key);
                  }}
                />
              </Form.Item>
            )}
          />
          {accountMode === 'public' && (
            <Controller
              control={control}
              rules={{
                required: '请输入',
              }}
              name="userPublicKeyFile"
              render={({ field, fieldState }) => (
                <Form.Item
                  status={formUtils.getStatus({
                    fieldState,
                    isValidating,
                    isSubmitted,
                  })}
                  label="用户公钥"
                >
                  <UploadFile
                    {...field}
                    accept=".key,.pem"
                    onSuccess={(key: File | null) => {
                      setValue(field.name, key, {
                        shouldValidate: true,
                      });
                      updateAddressByPuKeyFile(key);
                    }}
                  />
                </Form.Item>
              )}
            />
          )}
          {accountMode === 'permissionedWithCert' && (
            <Controller
              control={control}
              name="tlsEnable"
              render={({ field }) => (
                <Form.Item label={protocol === 'GRPC' ? '是否开启TLS' : <GlossaryGuide title={'是否开启TLS'} />}>
                  <Switch {...field} />
                </Form.Item>
              )}
            />
          )}
          {accountMode === 'permissionedWithCert' && tlsEnable && protocol === 'GRPC' && (
            <>
              <Controller
                control={control}
                name="hostName"
                render={({ field, fieldState }) => (
                  <Form.Item
                    status={formUtils.getStatus({
                      fieldState,
                      isValidating,
                      isSubmitted,
                    })}
                    message={fieldState.error?.message}
                    label={<GlossaryGuide title={'TLS_Host_Name'} />}
                  >
                    <Input size={'full'} {...field} />
                  </Form.Item>
                )}
              />
              <Controller
                control={control}
                rules={{
                  required: '请输入',
                }}
                name="userTLSCrtFile"
                render={({ field, fieldState }) => (
                  <Form.Item
                    status={formUtils.getStatus({
                      fieldState,
                      isValidating,
                      isSubmitted,
                    })}
                    label="用户TLS证书"
                  >
                    <UploadFile
                      {...field}
                      accept=".crt"
                      onSuccess={(key: File | null) => {
                        setValue(field.name, key, {
                          shouldValidate: true,
                        });
                      }}
                    />
                  </Form.Item>
                )}
              />
              <Controller
                control={control}
                rules={{
                  required: '请输入',
                }}
                name="userTLSKeyFile"
                render={({ field, fieldState }) => (
                  <Form.Item
                    status={formUtils.getStatus({
                      fieldState,
                      isValidating,
                      isSubmitted,
                    })}
                    label="用户TLS私钥"
                  >
                    <UploadFile
                      {...field}
                      accept=".key"
                      onSuccess={(key: File | null) => {
                        setValue(field.name, key, {
                          shouldValidate: true,
                        });
                      }}
                    />
                  </Form.Item>
                )}
              />
            </>
          )}
          <Controller
            control={control}
            name="browserLink"
            render={({ field, fieldState }) => (
              <Form.Item
                status={formUtils.getStatus({
                  fieldState,
                  isValidating,
                  isSubmitted,
                })}
                label={<GlossaryGuide title={'区块链浏览器链接（选填）'} />}
                message={fieldState.error?.message}
              >
                <Input size={'full'} {...field} />
              </Form.Item>
            )}
          />
        </Form>
        {initialized === false ? (
          <>
            <Button
              type={'primary'}
              disabled={!isValid}
              loading={loading}
              onClick={() => {
                handleClick();
              }}
              className={'mt-12n btn-lg'}
            >
              完成
            </Button>
          </>
        ) : (
          <footer className={'mt-12n btn-group'}>
            <Button type={'weak'} onClick={handleCancel}>
              取消
            </Button>
            <Button
              type={'primary'}
              disabled={!isValid}
              onClick={() => {
                handleClick();
              }}
              loading={loading}
            >
              添加
            </Button>
          </footer>
        )}
        <ConfirmModal title={'系统提示'} ref={httpFailRef} isHideConfirmBtn={true} cancelBtnText={'关闭'}>
          {'https请求失败，请检测tls证书是否被信任'}
        </ConfirmModal>
        <ConfirmModal title={'系统提示'} ref={chainFailRef} isHideConfirmBtn={true} cancelBtnText={'关闭'}>
          {'添加链配置失败，请检测长安链节点配置是否支持HTTP协议、TLS验证模式是否配置正确'}
        </ConfirmModal>
      </DetailPage>
    </>
  );
}

export default ChainNewPage;
