/*
 *
 *  Copyright (C) THL A29 Limited, a Tencent company. All rights reserved.
 *  SPDX-License-Identifier: Apache-2.0
 *
 */

import React, { useEffect, useRef, useState } from 'react';
import { useLocation, useNavigate } from 'react-router-dom';
import { Chain, ChainForm, ChainProtocol, PluginSetting } from '../utils/interface';
import { DetailPage, ReadonlyFormItem } from '../utils/common';
import { Button, Form, Input, Select, Switch, message } from 'tea-component';
import { isSupportHTTP, pick, protocolOptions, uploadCerts } from '../utils/utils';
import GlossaryGuide from '../utils/glossary-guide';
import chainStorageUtils from '../utils/storage';
import { initChainSubscribe, pullRemoteChainConfig } from '../services/chain';
import { useChainStore } from './popup';
import { Controller, useForm, useWatch } from 'react-hook-form';
import formUtils, { Validator } from '../utils/form-utils';
import { protocolExtraMap } from './chain-new-page';
import { UploadFile } from '../utils/upload-file';

function ChainUpdatePage() {
  const location = useLocation();
  const navigate = useNavigate();
  const { setChains, selectedChain, setSelectedChain } = useChainStore();
  const chain = location.state as Chain;
  const [pluginSetting, setPluginSetting] = useState<PluginSetting>();
  const {
    control,
    formState: { isValidating, isSubmitted },
    getValues,
    setValue,
  } = useForm({
    mode: 'onBlur',
    defaultValues: {
      tlsEnable: chain.tlsEnable || false,
      protocol: chain.protocol || 'GRPC',
      hostName: chain.hostName,
      browserLink: chain.browserLink,
      chainName: chain.chainName,
      nodeIp: chain.nodeIp,
    } as any,
  });
  const tlsEnable: ChainForm['tlsEnable'] = useWatch({ name: 'tlsEnable', control });
  const protocol: ChainProtocol = useWatch({ name: 'protocol', control });
  const [loading, setLoading] = useState(false);
  const refTlsCrt = useRef(null);
  const refTlsKey = useRef(null);
  const [defaultTLSKey, setDefaultTLSKey] = useState(null);
  const [defaultTLSCrt, setDefaultTLSCrt] = useState(null);
  useEffect(() => {
    // 获取代理配置
    chainStorageUtils.getSetting().then((res) => {
      setPluginSetting(res);
    });

    (async function () {
      // 设置默认tls
      if (chain.userTLSCrtFilePath && chain.userTLSKeyFilePath) {
        const [userTLSCrtFile, userTLSKeyFile] = await chainStorageUtils.getUploadFiles([
          chain.userTLSCrtFilePath,
          chain.userTLSKeyFilePath,
        ]);
        refTlsCrt.current?.setDefaultValue(userTLSCrtFile);
        refTlsKey.current?.setDefaultValue(userTLSKeyFile);
        setDefaultTLSCrt(userTLSCrtFile);
        setDefaultTLSKey(userTLSKeyFile);
      }
    })();
  }, []);
  const handleSave = () => {
    setLoading(true);
    const values = getValues() as ChainForm;
    const nextChain = {
      ...chain,
      ...pick(values, ['chainName', 'nodeIp', 'tlsEnable', 'hostName', 'protocol', 'browserLink']),
      httpSupport: isSupportHTTP(values),
    };
    (async function () {
      // 获取链账户， 获取连配置，并更新
      const accounts = await chainStorageUtils.getChainAccounts(nextChain.chainId);
      const account = accounts[0];
      try {
        if (account) {
          if (
            nextChain.accountMode === 'permissionedWithCert' &&
            nextChain.tlsEnable &&
            nextChain.protocol === 'GRPC'
          ) {
            // 通信grpc， 开启tls，cert需要tls证书，私钥
            // 1. 上传新的
            console.log(
              'values.userTLSKeyFile',
              values.userTLSKeyFile === defaultTLSKey,
              values.userTLSCrtFile === defaultTLSCrt,
            );
            if (!values.userTLSKeyFile || !values.userTLSCrtFile) {
              setLoading(false);
              return message.error({ content: 'cert模式链使用gPRC通信开启tls后，需要提供TLS证书和TLS私钥' });
            }
            if (values.userTLSKeyFile !== defaultTLSKey || values.userTLSCrtFile !== defaultTLSCrt) {
              // 上传替换
              const { crtFileName, keyFileName } = await uploadCerts(values.userTLSKeyFile, values.userTLSCrtFile);
              const [userTLSKeyFilePath, userTLSCrtFilePath] = await chainStorageUtils.uploadFiles([
                values.userTLSKeyFile,
                values.userTLSCrtFile,
              ]);
              nextChain.userTLSKeyFilePath = userTLSKeyFilePath;
              nextChain.userTLSCrtFilePath = userTLSCrtFilePath;
              nextChain.userTLSCrtFile = crtFileName;
              nextChain.userTLSKeyFile = keyFileName;
              nextChain.proxyURL = pluginSetting?.proxyHostnameTls;
            }
            // 3.不上传新的，有老的用老的
          }
          // 如果tls更新，需要重新上传
          const { updatedChain, chains } = (await pullRemoteChainConfig(nextChain, account)) || {};
          console.log('updatedChain, chains ', updatedChain, chains);
          if (updatedChain && chains) {
            setChains(chains);
            if (selectedChain.chainId === nextChain.chainId) {
              setSelectedChain(updatedChain);
            }
            await initChainSubscribe(updatedChain);
            // 自动选链
            console.log('updatedChain', updatedChain);
            navigate(`/chains/${nextChain.chainId}`, {
              state: updatedChain,
            });
          } else {
            throw new Error(`获取链配置失败${nextChain.chainId}`);
          }
        }
      } catch (error) {
        console.error(error);
        message.error({ content: '修改区块链网络信息失败，请检查所填写的信息是否有误后重试' });
      }
      setLoading(false);
    })();
  };

  return (
    <DetailPage title={'区块链网络详情'} backUrl={`/chains/${chain.chainId}`} backState={chain}>
      <Form layout={'vertical'}>
        <ReadonlyFormItem label={<GlossaryGuide title={'区块链ID'} />} text={chain.chainId} />

        <Controller
          control={control}
          rules={{
            required: '请输入',
          }}
          name="chainName"
          render={({ field, fieldState }) => (
            <Form.Item
              status={formUtils.getStatus({
                fieldState,
                isValidating,
                isSubmitted,
              })}
              message={fieldState.error?.message}
              label={<GlossaryGuide title={'区块链网络名称'} />}
            >
              <Input size={'full'} {...field} />
            </Form.Item>
          )}
        />
        <Controller
          control={control}
          rules={{
            validate: Validator.validateNodeIP,
          }}
          name="nodeIp"
          render={({ field, fieldState }) => (
            <Form.Item
              status={formUtils.getStatus({
                fieldState,
                isValidating,
                isSubmitted,
              })}
              message={fieldState.error?.message}
              label={<GlossaryGuide title={'节点RPC服务地址'} />}
            >
              <Input size={'full'} placeholder={'127.0.0.1:8080'} {...field} />
            </Form.Item>
          )}
        />
        <Controller
          control={control}
          name="protocol"
          render={({ field }) => (
            <Form.Item label={<GlossaryGuide title={'网络通讯方式'} />} extra={protocolExtraMap[protocol]}>
              <Select
                size={'full'}
                matchButtonWidth
                appearance="button"
                options={protocolOptions}
                placeholder="网络通讯方式"
                {...field}
              />
            </Form.Item>
          )}
        />
        {chain.accountMode === 'permissionedWithCert' && (
          <Controller
            control={control}
            name="tlsEnable"
            render={({ field }) => (
              <Form.Item label={protocol === 'GRPC' ? '是否开启TLS' : <GlossaryGuide title={'是否开启TLS'} />}>
                <Switch {...field} />
              </Form.Item>
            )}
          />
        )}
        {chain.accountMode === 'permissionedWithCert' && tlsEnable && protocol === 'GRPC' && (
          <>
            <Controller
              control={control}
              name="hostName"
              render={({ field, fieldState }) => (
                <Form.Item
                  status={formUtils.getStatus({
                    fieldState,
                    isValidating,
                    isSubmitted,
                  })}
                  message={fieldState.error?.message}
                  label={<GlossaryGuide title={'TLS_Host_Name'} />}
                >
                  <Input size={'full'} {...field} />
                </Form.Item>
              )}
            />
            <Controller
              control={control}
              name="userTLSCrtFile"
              render={({ field, fieldState }) => (
                <Form.Item
                  status={formUtils.getStatus({
                    fieldState,
                    isValidating,
                    isSubmitted,
                  })}
                  label="用户TLS证书"
                >
                  <UploadFile
                    {...field}
                    accept=".crt"
                    ref={refTlsCrt}
                    onSuccess={(key: File | null) => {
                      setValue(field.name, key, {
                        shouldValidate: true,
                      });
                    }}
                  />
                </Form.Item>
              )}
            />
            <Controller
              control={control}
              name="userTLSKeyFile"
              render={({ field, fieldState }) => (
                <Form.Item
                  status={formUtils.getStatus({
                    fieldState,
                    isValidating,
                    isSubmitted,
                  })}
                  label="用户TLS私钥"
                >
                  <UploadFile
                    {...field}
                    ref={refTlsKey}
                    accept=".key"
                    onSuccess={(key: File | null) => {
                      setValue(field.name, key, {
                        shouldValidate: true,
                      });
                    }}
                  />
                </Form.Item>
              )}
            />
          </>
        )}
        <Controller
          control={control}
          name="browserLink"
          render={({ field, fieldState }) => (
            <Form.Item
              status={formUtils.getStatus({
                fieldState,
                isValidating,
                isSubmitted,
              })}
              label={<GlossaryGuide title={'区块链浏览器链接（选填）'} />}
              message={fieldState.error?.message}
            >
              <Input size={'full'} {...field} />
            </Form.Item>
          )}
        />
      </Form>
      <footer>
        {/* <div className="signature-options"> */}

        <Button type={'primary'} loading={loading} className={'btn-lg'} onClick={handleSave}>
          完成
        </Button>
        {/* <Button type={'weak'} onClick={handleRefresh}>
          刷新
        </Button>
        <Button type={'primary'} onClick={handleEdit}>
          修改
        </Button> */}
      </footer>
    </DetailPage>
  );
}

export default ChainUpdatePage;
