/*
 *
 *  Copyright (C) THL A29 Limited, a Tencent company. All rights reserved.
 *  SPDX-License-Identifier: Apache-2.0
 *
 */

import React, { useEffect, useState } from 'react';
import './account.less';
import ChainsPage from './chains-page';
import { MemoryRouter as Router, NavigateFunction, Route, Routes, useLocation, useNavigate } from 'react-router-dom';
import LoginPage from './login-page';
import ChainNewPage from './chain-new-page';
import ChainDetailPage from './chain-detail-page';
import chainStorageUtils from '../utils/storage';
import AccountsPage from './accounts-page';
import AccountNewPage from './account-new-page';
import SignaturePage from './signature-page';
import TxLogsPage from './tx-logs-page';
import SettingPage from './setting-page';
import LoginLockPage from './lock-page';
import HomePage from './home/home-page';
import { Account, Chain, Wallet } from '../utils/interface';
import { ChainPage } from '../utils/common';
import create from 'zustand';
import AboutPage from './about-page';
import SubscribeContractPage from './subscribe-contract-page';
import { ContractDetailPage } from './subscribe-detail/contract-detail';
import { TransactionDetailPage } from './subscribe-detail/transaction-detail';
import AccountConnect from './account-connect';
import AccountSignature from './account-signature';
import AccountConnectSetting from './account-connect-setting';
import '../iconsvg/index';
import { SendRequestParam } from '../event-page';
import { responseAccountInfo } from '../utils/utils';
import { getNowSeconds, sendMessageToContentScript } from '../utils/tools';
import deepEqual from 'deep-equal';
import ChainImportPage from './chain-import-page';
import AccountImportPage from './account-import-page';
import { checkUpdateExpiredOfficialChainData } from '../utils/official-chain';
import ChainUpdatePage from './chain-update-page';
import HdWalletCreateByMnemonic from './wallet/hd-wallet-create-by-mnemonic';
import HdWalletImportByMnemonic from './wallet/hd-wallet-import-by-mnemonic';
import HdWalletDetail from './wallet/hd-wallet-detail';
import HdWalletMnemonicDetail from './wallet/hd-wallet-mnemonic-detail';
import JbokWalletAccountImportByMnemonic from './wallet/jbok-wallet-account-import-by-mnemonic';
import JbokWalletDetail from './wallet/jbok-wallet-detail';
import WalletAccountDetail from './wallet/wallet-account-detail';
import '../beacon';
import { VcDetailPage } from './did/vc-detail';
import { DidDetailPage } from './did/did-detail';
import DidAuthority from './did/authority-did';
import VcAuthority from './did/authority-vc';
import AccountExportToApp from './account-export-to-app';
/**
 * @description
 */
chrome.runtime.connect({ name: 'popup' });

const initState: {
  chains: Chain[];
  selectedChain: Chain;
  currentAccount: Account;
  initialized: boolean;
  currentTab: chrome.tabs.Tab;
  currentWallet: Wallet;
} = {
  chains: [],
  selectedChain: null,
  currentAccount: null,
  /**
   *@description 初始化完成标记位，用户从登录页进入添加网络页面，完成首次网络添加后，标记位恢复为true
   */
  initialized: true,
  currentTab: null,
  currentWallet: null,
};

const OPERATION_NAV_MAP = {
  createUserContract: '/signature',
  invokeUserContract: '/signature',
  openConnect: '/accounts/connect',
  openAccountExport: '/accounts/export-to-app',
  openAccountSignature: '/accounts/signature',
  importChainConfig: '/chains/import',
  importAccountConfig: '/accounts/import',
  importSubscribeContract: '/subscribe-contract/new',
  openDidAuthority: '/did/authority-did',
  openVcAuthority: '/did/authority-vc',
};

export const navByMessage = (nav: NavigateFunction, temp: SendRequestParam) => {
  const navPath = OPERATION_NAV_MAP[temp.operation] || '/';
  nav(navPath, {
    state: temp,
  });
};

type Tab = chrome.tabs.Tab & { host?: string };

/**
 * 全局状态上下文
 */
export const useChainStore = create<{
  chains: Chain[];
  selectedChain: Chain;
  currentAccount?: Account;
  initialized: boolean;
  currentTab: Tab;
  currentWallet: Wallet;
  setChains: (chains: Chain[]) => void;
  setSelectedChain: (chains: Chain) => void;
  setCurrentAccount: (account: Account) => void;
  updateSelectData: ({ chain, account }: { chain?: Chain; account?: Account }) => Promise<void>;
  reset: () => void;
  setInitialized: (b: Boolean) => void;
  setCurrentTab: (tab?: Tab) => void;
  setCurrentWallet: (wallet: Wallet) => void;
}>((set: any, get) => ({
  ...initState,
  setChains: (chains) =>
    set((state) => ({
      ...state,
      chains,
    })),
  async updateSelectData({ chain, account }) {
    let { currentAccount, selectedChain } = get();
    if (chain && chain?.chainId !== selectedChain.chainId) {
      await chainStorageUtils.setSelectedChain(chain);
      selectedChain = chain;
    }
    if (account && account?.address !== currentAccount?.address) {
      await chainStorageUtils.setCurrentAccount(account.address);
      currentAccount = account;
    }
    set((state) => ({
      ...state,
      selectedChain,
      currentAccount,
    }));
  },
  // 添加链账户，并同步到storage，如果没有当前账户就设置
  async addChainAccount({ chain, account }: { chain: Chain; account: Account }) {
    const { currentAccount } = get();
    await chainStorageUtils.addChainAccount(chain.chainId, account);
    if (!currentAccount) {
      const current = await chainStorageUtils.setCurrentAccount(account.address);
      set((state) => ({
        ...state,
        currentAccount: current,
      }));
    }
  },
  setSelectedChain: (chain) =>
    set((state) => ({
      ...state,
      selectedChain: chain,
    })),
  setCurrentAccount: (account) =>
    set((state) => {
      if (!deepEqual(account, state.currentAccount)) {
        const { chainName, chainId } = state.selectedChain || {};
        sendMessageToContentScript({
          operation: 'changeCurrentAccount',
          data: {
            status: 'done',
            timestamp: getNowSeconds(),
            info: {
              accounts: responseAccountInfo([account], state.currentTab?.host),
              chain: { chainName, chainId },
            },
          },
        });
      }
      return {
        ...state,
        currentAccount: account,
      };
    }),
  reset: () => set(() => initState),
  setInitialized: (b: boolean) =>
    set((state) => ({
      ...state,
      initialized: b,
    })),
  setCurrentTab: (tab: Tab) =>
    set((state) => ({
      ...state,
      currentTab: tab,
    })),
  setCurrentWallet: (wallet: Wallet) =>
    set((state) => ({
      ...state,
      currentWallet: wallet,
    })),
}));
/**
 * @description 全局上下文
 */
export const AppContext = React.createContext<{
  isLoggedIn: boolean;
}>({ isLoggedIn: false });

export default function Popup() {
  return (
    <Router>
      <CustomRoutes />
    </Router>
  );
}

function CustomRoutes() {
  const navigate = useNavigate();
  const { pathname } = useLocation();

  const { setSelectedChain, setChains, setCurrentTab, setCurrentAccount, selectedChain, setCurrentWallet } =
    useChainStore();
  const [hasInit, setHasInit] = useState<boolean>(false);
  const [isLoggedIn, setIsLoggedIn] = useState<boolean>(false);
  useEffect(() => {
    const hideChainSelectPage = [
      '/login',
      '/login-lock',
      '/signature',
      '/accounts/connect',
      '/accounts/export-to-app',
      '/accounts/signature',
      '/did/authority-did',
      '/did/authority-vc',
    ];
    setIsLoggedIn(hideChainSelectPage.indexOf(pathname) === -1);
  }, [pathname]);
  useEffect(() => {
    const checkFn = async () => {
      const [loginRes, loginLifeRes, tempOperationRes] = await Promise.all([
        chainStorageUtils.getLogin(),
        chainStorageUtils.getLoginLife(),
        chainStorageUtils.getTempOperation(),
      ]);
      setHasInit(true);
      // eslint-disable-next-line @typescript-eslint/no-misused-promises
      chrome.tabs.query({ active: true }, async (tabs) => {
        const tab = tabs.find((ele) => ele.id === tempOperationRes?.tabId);
        // const tab = tabs[0];
        // eslint-disable-next-line @typescript-eslint/consistent-type-assertions
        if (!tab) setCurrentTab({} as Tab);
        // eslint-disable-next-line no-useless-escape
        const host = tab?.url?.match(/^https?:\/\/[^\/]+/)?.[0];
        if (host) {
          setCurrentTab({
            ...tab,
            host,
          });
        }
        if (!loginRes) {
          return navigate('/login');
        }
        if (!loginLifeRes) {
          await chainStorageUtils.setLoginLock();
          return navigate('/login-lock', {
            state: tempOperationRes,
          });
        }
        if (tempOperationRes) {
          navByMessage(navigate, tempOperationRes);
        } else {
          return navigate('/');
        }
      });
    };

    Promise.all([
      chainStorageUtils.getChains(),
      chainStorageUtils.getSelectedChain(),
      chainStorageUtils.getCurrentAccount(),
      chainStorageUtils.getCurrentWallet(),
    ])
      .then(async ([chains, selectedChain, currentAccount, currentWallet]) => {
        // 检测更新过期的测试链数据
        const { correctChains, correctSelectedChain } = await checkUpdateExpiredOfficialChainData({
          chains,
          selectedChain,
        });
        setSelectedChain(correctSelectedChain);
        setChains(correctChains);
        setCurrentAccount(currentAccount);
        setCurrentWallet(currentWallet);
      })
      .then(checkFn);
  }, []);

  useEffect(() => {
    chainStorageUtils.getCurrentAccount().then((account) => {
      console.debug('setCurrentAccount', account);
      setCurrentAccount(account);
    });
    chainStorageUtils.getCurrentWallet().then((wallet) => {
      console.debug('setCurrentWallet', wallet);
      setCurrentWallet(wallet);
    });
  }, [selectedChain]);

  return (
    <div className={'content'}>
      <AppContext.Provider
        value={{
          isLoggedIn,
        }}
      >
        {hasInit && (
          <ChainPage isLoggedIn={isLoggedIn}>
            <Routes>
              <Route path={'/'} element={<HomePage />} />
              <Route index element={<HomePage />} />
              {/* <Route path={'login'} element={<LoginPage onLogged={() => setIsLoggedIn(true)} />} />
          <Route path={'login-lock'} element={<LoginLockPage onClocked={() => setIsLoggedIn(true)} onReset={() => setIsLoggedIn(false)} />} /> */}
              <Route path={'login'} element={<LoginPage />} />
              <Route path={'login-lock'} element={<LoginLockPage />} />
              {/* <Route index element={<SignaturePage/>}/> */}
              <Route path={'signature'} element={<SignaturePage />} />
              <Route path={'tx-logs'} element={<TxLogsPage />} />
              <Route path={'chains'}>
                <Route index element={<ChainsPage />} />
                <Route path={'new'} element={<ChainNewPage />} />
                {/* 导入 */}
                <Route path={'import'} element={<ChainImportPage />} />
                <Route path={':id'} element={<ChainDetailPage />} />
                <Route path={'update'} element={<ChainUpdatePage />} />
              </Route>
              <Route path={'accounts'}>
                <Route index element={<AccountsPage />} />
                <Route path={'new'} element={<AccountNewPage />} />
                {/* 导入 */}
                <Route path={'import'} element={<AccountImportPage />} />
                <Route path={'connect'} element={<AccountConnect />} />
                <Route path={'export-to-app'} element={<AccountExportToApp />} />
                <Route path={'signature'} element={<AccountSignature />} />
                <Route path={'connect-setting'} element={<AccountConnectSetting />} />
              </Route>
              <Route path={'setting'} element={<SettingPage onReset={() => setIsLoggedIn(false)} />} />
              <Route path={'about-us'} element={<AboutPage />} />

              <Route path={'subscribe-contract'}>
                <Route path={'new'} element={<SubscribeContractPage />} />
                <Route path={'contract-detail'} element={<ContractDetailPage />} />
                <Route path={'transaction-detail'} element={<TransactionDetailPage />} />
              </Route>

              <Route path={'wallet'}>
                <Route path={'hd-wallet-create-by-mnemonic'} element={<HdWalletCreateByMnemonic />} />
                <Route path={'hd-wallet-import-by-mnemonic'} element={<HdWalletImportByMnemonic />} />
                <Route path={'hd-wallet-detail'} element={<HdWalletDetail />} />
                <Route path={'hd-wallet-mnemonic-detail'} element={<HdWalletMnemonicDetail />} />
                <Route
                  path={'jbok-wallet-account-import-by-mnemonic'}
                  element={<JbokWalletAccountImportByMnemonic />}
                />
                <Route path={'jbok-wallet-detail'} element={<JbokWalletDetail />} />
                <Route path={'wallet-account-detail'} element={<WalletAccountDetail />} />
              </Route>
              <Route path={'did'}>
                <Route path={'did-detail'} element={<DidDetailPage />} />
                <Route path={'vc-detail'} element={<VcDetailPage />} />
                <Route path={'authority-did'} element={<DidAuthority />} />
                <Route path={'authority-vc'} element={<VcAuthority />} />
              </Route>
            </Routes>
          </ChainPage>
        )}
      </AppContext.Provider>
    </div>
  );
}
