/*
 *
 *  Copyright (C) THL A29 Limited, a Tencent company. All rights reserved.
 *  SPDX-License-Identifier: Apache-2.0
 *
 */

import { TabIdentifierClient } from 'chrome-tab-identifier';

console.log('content-script loaded.');
const tabIdClient = new TabIdentifierClient();

/**
 * 关于message类型定义
 * @see ExtensionResponse
 */
chrome.runtime.onMessage.addListener(
  (message) => {
    if (message.operation === 'debug') {
      console.log(`%c${message.data.detail}`, 'color:red');
    } else {
      window.postMessage(message, '*');
    }
    return true;
  },
);

window.addEventListener('message', function (event) {
  if (event.data.operation) {
    tabIdClient.getTabId().then(tabId => {
      chrome.runtime.sendMessage({
        ...event.data,
        tabId,
      });
    });
  }
});

function injectScript(file, node) {
  const th = document.getElementsByTagName(node)[0];
  const s = document.createElement('script');
  s.setAttribute('type', 'text/javascript');
  s.setAttribute('src', file);
  th.appendChild(s);
}

injectScript(chrome.runtime.getURL('js/sdk.js'), 'body');
