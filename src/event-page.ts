/*
 *
 *  Copyright (C) THL A29 Limited, a Tencent company. All rights reserved.
 *  SPDX-License-Identifier: Apache-2.0
 *
 */

// Listen to messages sent from other parts of the extension.
import chainStorageUtils, { settingStoreUtils } from './utils/storage';
import { Chain, InvokeUserContract, UserContract } from './utils/interface';
import { TabIdentifier } from 'chrome-tab-identifier';
import * as common_result_pb from '../web-sdk/grpc-web/common/result_pb';
import { getNowSeconds, sendMessageToContentScript } from './utils/tools';

type ResponseStatus = 'info' | 'error' | 'warn' | 'success' | 'done';
export type ChainMakerTicket = any;
export type ChainMakerRequest<T, Y> = {
  operation: Y;
  /**
   * 网页透传票据信息，插件本身不做任何处理，只在消息返回时携带
   * 记录事件ID，比operation更为精细，一个event可以接收多次response返回
   */
  ticket?: ChainMakerTicket;
  /**
   * 链id
   */
  chainId?: string;
  /**
   * 用户地址
   */
  accountAddress?: string;
  body?: T;
  tabId?: number;
};

/**
 * 创建用户合约
 */
export type CreateUserContractRequest = ChainMakerRequest<UserContract, 'createUserContract'>;
/**
 * 调用用户合约
 */
export type InvokeUserContractRequest = ChainMakerRequest<InvokeUserContract, 'invokeUserContract'>;

/**
 * 导入链
 */
export type SendRequestImportChainConfig = ChainMakerRequest<Chain, string>;

/**
 * 导入账户
 */

interface AccountParams {
  // 文件名称
  userSignKeyName?: string;
  userSignCrtName?: string;
  userPublicKeyName?: string;
  // 文件内容
  userSignKeyContent: string;
  userSignCrtContent: string;
  userPublicKeyContent: string;
  name: string;
  crtName?: string;
  address?: string;
  orgId: string;
}

/**
 * 账户签名
 */
export interface AccountSignParams {
  /**
   * 返回编码格式
   * @default 'base64'
   */
  resCode?: 'hex' | 'base64';
  /**
   * 待签名十六进制字符
   * @default 'base64'
   */
  hexContent: string;
  /**
   * 指定算法，目只支持知道sm；，默认会根据私钥header信息和 ec椭圆曲线算法参数识别
   */
  alg?: 'sm2';
}

/**
 * did授权
 */
export interface DidAuthorityParams {
  /**
   * 链id
   */
  chainId: string;
  /**
   * 签名随机数
   */
  vp: string;
  /**
   * vp验证者did（dapp did）
   */
  verifier?: string;
}
export interface VcAuthorityParams extends DidAuthorityParams {
  /**
   * 指定需要授权的did
   */
  did?: string;
  /**
   * 指定vc类型
   * '100000': 个人实名认证
   * '100001': 企业实名认证
   */
  templateId?: '100000' | '100001';
}

export interface VerifyAccounts {
  /**
   * 链id
   */
  chainId: string;
  /**
   * 待校验账户地址列表
   */
  addressList: string[];
}

export type SendRequestAccountSignParams = ChainMakerRequest<AccountSignParams, string>;

export type SendRequestImportAccountConfig = ChainMakerRequest<AccountParams, string>;

export type SendRequestImportSubscribeContract = ChainMakerRequest<
  {
    chainId: string;
    contractName: string;
    contractType: ContractType;
  },
  string
>;
/**
 * 调用通用
 */
export type SendRequestParam = ChainMakerRequest<any, string>;

/**
 * 拓展程序回复网页消息结构
 */
export interface ExtensionResponse {
  /**
   * 记录来自哪个操作的响应
   */

  operation?: string;

  ticket?: ChainMakerTicket;
  /**
   * 链id
   */
  chainId?: string;
  /**
   * 用户地址
   */
  accountAddreess?: string;

  data?: {
    /**
     * 如果是done/error，则为该ticket的最后一条消息，之后不会有任何消息发送
     */
    status: ResponseStatus;

    /**
     * 单位秒，针对交易后发回的时间为准确的出块时间等价于如下字段
     * @see TxLog.timestamp
     */
    timestamp?: number;

    /**
     * 详情，针对status=success时显示的信息为chainNodeResult.contractResult.message
     */
    detail?: string;

    /**
     * 链节点交易返回完整信息，这里透传暴露给接入dapp
     */
    chainNodeResult?: common_result_pb.Result.AsObject;

    /**
     *  其他 消息数据
     */
    info?: any;
  };
}

type RuntimeMessageRequest = ChainMakerRequest<any, any> & {
  tabId: number;
  from?: string;
};
interface MessageResults {
  message?: string;
  info?: MessageInfo;
  status?: ResponseStatus;
}

type MessageInfo = {
  // 成功，取消，错误，超时
  code: 0 | 1 | 2 | 3;
  // 错误信息
  res?: string;
  // 接口自定义返回
  [key: string]: any;
};
new TabIdentifier();
const openPopup = async (request: RuntimeMessageRequest) => {
  await chainStorageUtils.setTempOperation(request);
  return new Promise((resolve, reject) => {
    chrome.windows.create(
      {
        url: `popup.html`,
        type: 'popup',
        height: 620,
        width: 357,
        left: 100,
        top: 100,
        focused: true,
      },
      // eslint-disable-next-line @typescript-eslint/no-misused-promises
      async (v) => {
        try {
          // HACK
          chrome.runtime.getPlatformInfo((info) => {
            if (info.os === 'win') {
              // win下窗体宽度初始化时不正确
              chrome.windows.update(v.id, { width: 357 + 16 });
            }
          });
          const existVid = await settingStoreUtils.getWindowId();
          existVid &&
            chrome.windows.get(existVid, (window) => {
              window && chrome.windows.remove(existVid);
            });
          await settingStoreUtils.setWindowId(v.id);
          resolve(existVid);
        } catch (error) {
          reject(error);
        }
      },
    );
  });
};

type Operationhandle = (request: RuntimeMessageRequest) => Promise<MessageResults | void>;
const defaultMessageResults: MessageResults = {
  message: '插件已唤起，请在插件继续操作',
  status: 'info',
};
class ChainMakerOperationHandler {
  handles: Record<string, Operationhandle> = {};
  register(name: string, handle: Operationhandle) {
    const nameList = name.replace(/\s/g, '').split(',');
    nameList.forEach((name) => {
      this.handles[name] = handle;
    });
  }
  async bootstrap(request: RuntimeMessageRequest): Promise<MessageResults> {
    try {
      if (typeof this.handles[request.operation] !== 'function') {
        throw new Error(`operation '${request.operation}' 未注册`);
      }
      const result = (await this.handles[request.operation](request)) || {};
      return { ...defaultMessageResults, ...result };
    } catch (error) {
      return {
        status: 'error',
        message: String(error),
      };
    }
  }
}
const chainMakerOperationHandler = new ChainMakerOperationHandler();
chainMakerOperationHandler.register('debug', async (request) => {
  if (!request.body) {
    return {
      message: 'request.body不能为空',
      status: 'error',
    };
  }
  const value = request.body.debug;
  await settingStoreUtils.setDebug(value);
  return {
    status: 'success',
    message: value ? 'debug模式开启，请小心操作' : 'debug模式已关闭',
  };
});
chainMakerOperationHandler.register('createUserContract,invokeUserContract', async (request) => {
  await openPopup(request);
  return {
    message: '插件已唤起，请在插件上选择网络/账户发起上链请求',
  };
});

chainMakerOperationHandler.register('openConnect,openAccountSignature,openAccountExport', async (request) => {
  await openPopup(request);
  return {
    message: '插件已唤起，请在插件上选择连接的账户',
  };
});
chainMakerOperationHandler.register('importChainConfig,importAccountConfig', async (request) => {
  await openPopup(request);
});

chainMakerOperationHandler.register('importSubscribeContract', async (request) => {
  if (!request.body?.contractName || !request.body.chainId) {
    const msg = 'contractName、chainId不能为空';
    return {
      status: 'done',
      message: msg,
      info: {
        code: 2,
        res: msg,
      },
    };
  }
  await openPopup(request);
});

chainMakerOperationHandler.register('getConnectAccounts', async () => {
  const info: MessageInfo = await new Promise((resolve) => {
    // eslint-disable-next-line @typescript-eslint/no-misused-promises
    chrome.tabs.query({ active: true }, async (tabs) => {
      try {
        const host = tabs[0].url?.match(/^https?:\/\/[^\/]+/)[0];
        const currentChainAccounts = await chainStorageUtils.getCurrentChainAccounts();
        const selectedChain = await chainStorageUtils.getSelectedChain();
        const accounts = currentChainAccounts
          .filter((item) => item.authHosts?.indexOf(host) > -1)
          .map((ac) => {
            const { color, address, isCurrent } = ac;
            return { color, address, isCurrent };
          });
        const { chainName, chainId } = selectedChain;
        resolve({
          code: 0,
          accounts,
          chain: { chainName, chainId },
        });
      } catch (error) {
        resolve({
          code: 2,
          accounts: [],
          chain: {},
          res: error.message,
        });
      }
    });
  });
  return { info, status: 'done' };
});

chainMakerOperationHandler.register('openDidAuthority, openVcAuthority', async (request) => {
  await openPopup(request);
  return {
    message: '插件已唤起，请在插件上选择连接的账户',
  };
});

chainMakerOperationHandler.register('verifyAccounts', async (request) => {
  const info: MessageInfo = await new Promise((resolve) => {
    // eslint-disable-next-line @typescript-eslint/no-misused-promises

    (async () => {
      try {
        const { addressList, chainId } = request.body;
        const chainAccounts = await chainStorageUtils.getChainAccounts(chainId);

        const results = chainAccounts
          .filter((item) => addressList.indexOf(item.address) !== -1)
          .map((ac) => ac.address);
        resolve({
          code: 0,
          addressList: results,
        });
      } catch (error) {
        resolve({
          code: 2,
          addressList: [],
          res: error.message,
        });
      }
    })();
  });
  return { info, status: 'done' };
});

// eslint-disable-next-line @typescript-eslint/no-misused-promises, @typescript-eslint/no-unused-vars
chrome.runtime.onMessage.addListener(async (request: RuntimeMessageRequest, _sender, sendResponse) => {
  // onMessage must return "true" if response is async.
  if (!request?.tabId || request.from === 'chainmaker-plugin') {
    return true;
  }
  await chainStorageUtils.setActiveTabId(request.tabId);

  const { status, message, info } = await chainMakerOperationHandler.bootstrap(request);
  sendMessageToContentScript({
    operation: request.operation,
    data: {
      status,
      detail: message,
      info,
      timestamp: getNowSeconds(),
    },
    ticket: request.ticket,
  });

  return true;
});

chrome.runtime.onMessageExternal.addListener(
  // eslint-disable-next-line @typescript-eslint/no-unused-vars, @typescript-eslint/no-misused-promises
  async (_request, sender, sendResponse) =>
    // console.log(_request);
    true,
);

chrome.runtime.onConnect.addListener((port) => {
  if (port.name === 'popup') {
    // eslint-disable-next-line @typescript-eslint/no-misused-promises
    port.onDisconnect.addListener(async () => {
      const [login, lock] = await Promise.all([chainStorageUtils.getLogin(), chainStorageUtils.getLoginLock()]);
      if (login && !lock) {
        chainStorageUtils.setLoginLife();
      }
      // console.log('popup has been closed');
    });
  }
});
