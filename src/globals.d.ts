declare module '*.svg' {
  const content: any;
  export default content;
}

type ContractType = 'CMID' | 'CMDFA' | 'CMNFA' | 'GAS' | 'OTHER' | 'CMEVI';
type OfficialChainId = 'chainmaker_testnet_chain' | 'chainmaker_testnet_pk';
