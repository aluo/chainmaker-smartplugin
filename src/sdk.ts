/*
 *
 *  Copyright (C) THL A29 Limited, a Tencent company. All rights reserved.
 *  SPDX-License-Identifier: Apache-2.0
 *
 */

import md5 from 'md5';
import { ChainMakerRequest, CreateUserContractRequest, InvokeUserContractRequest, SendRequestParam } from './event-page';

async function handler(request: ChainMakerRequest<any, any>) {
  request.operation !== 'debug' && console.log('sdk call, params', request);
  window.postMessage(request, '*');
}

const createResponeMessage = () => {
  const map = {};
  function addResponseMessage(ticket: string, fn: (value: unknown) => void) {
    map[ticket] = fn;
  }
  window.addEventListener('message', (event) => {
    if (event.source !== window) {
      return;
    }
    // console.log("addEventListener response:", event);
    const { data, ticket, operation, } = event.data;
    if (map[ticket] && data?.status==="done") {
      map[ticket](data);
    }
    if (window.chainMaker?.event_list?.[operation]?.length) {
      // window.chainMaker.event_list?.[operation](data);
      window.chainMaker.emit(operation,data);
    }
  })
  return addResponseMessage;
}

const addResponseMessage = createResponeMessage();
window.chainMaker = {
  ...(window.chainMaker||{}),
  version: require('../package.json').version,
  createUserContract: (req: Omit<CreateUserContractRequest, 'operation'>) => {
    return handler({
      ...req,
      operation: 'createUserContract',
    });
  },
  invokeUserContract: (req: Omit<InvokeUserContractRequest, 'operation'>) => {
    return handler({
      ...req,
      operation: 'invokeUserContract',
    });
  },
  sendRequest: (operation: string, info?: any, callback?: (res: any) => void) => {
    const ticket = md5(Math.random().toString());
    let params;
    if (typeof operation !== 'string') {
      callback = info;
      params = operation;
    } else {
      params = {
        ...info,
        operation,
      }
    }
    handler({
      ...params,
      ticket,
    });
    if (typeof callback === 'function') {
      addResponseMessage(ticket, callback);
    }
  },
  set debug(debug: boolean) {
    handler({
      body: {
        debug,
      },
      operation: 'debug',
    });
  },
  event_list: {},
  // 订阅
  on(event, fn) {
    let _this = this;
    // 如果对象中没有对应的 event 值，也就是说明没有订阅过，就给 event 创建个缓存列表
    // 如有对象中有相应的 event 值，把 fn 添加到对应 event 的缓存列表里
    (_this.event_list[event] || (_this.event_list[event] = [])).push(fn);
    return _this;
  },
  // 监听一次
  once(event, fn) {
    // 先绑定，调用后删除
    let _this = this;
    function on(data) {
      _this.off(event, on);
      fn.apply(_this, [data]);
    }
    on.fn = fn;
    _this.on(event, on);
    return _this;
  },
  // 取消订阅
  off(event, fn) {
    let _this = this;
    let fns = _this.event_list[event];
    // 如果缓存列表中没有相应的 fn，返回false
    if (!fns) return false;
    if (!fn) {
      // 如果没有传 fn 的话，就会将 event 值对应缓存列表中的 fn 都清空
      fns && (fns.length = 0);
    } else {
      // 若有 fn，遍历缓存列表，看看传入的 fn 与哪个函数相同，如果相同就直接从缓存列表中删掉即可
      let cb;
      for (let i = 0, cbLen = fns.length; i < cbLen; i++) {
        cb = fns[i];
        if (cb === fn || cb.fn === fn) {
          fns.splice(i, 1);
          break
        }
      }
    }
    return _this;
  },
  // 发布
  emit(event,data) {
    let _this = this;
    // 第一个参数是对应的 event 值，直接用数组的 shift 方法取出
    let fns = [..._this.event_list[event]];
    // 如果缓存列表里没有 fn 就返回 false
    if (!fns || fns.length === 0) {
      return false;
    }
    // 遍历 event 值对应的缓存列表，依次执行 fn
    fns.forEach(fn => {
      fn.apply(_this, [data]);
    });
    return _this;
  }
};
// window.chainMaker.emit("load");
window.chainMaker?.onLoad?.();
