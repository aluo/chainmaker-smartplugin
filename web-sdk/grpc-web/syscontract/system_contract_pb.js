// source: syscontract/system_contract.proto
/**
 * @fileoverview
 * @enhanceable
 * @suppress {missingRequire} reports error on implicit type usages.
 * @suppress {messageConventions} JS Compiler reports an error if a variable or
 *     field starts with 'MSG_' and isn't a translatable message.
 * @public
 */
// GENERATED CODE -- DO NOT EDIT!
/* eslint-disable */
// @ts-nocheck

var jspb = require('google-protobuf');
var goog = jspb;
var global = (function() { return this || window || global || self || Function('return this')(); }).call(null);

goog.exportSymbol('proto.syscontract.SystemContract', null, global);
/**
 * @enum {number}
 */
proto.syscontract.SystemContract = {
  CHAIN_CONFIG: 0,
  CHAIN_QUERY: 1,
  CERT_MANAGE: 2,
  GOVERNANCE: 3,
  MULTI_SIGN: 4,
  CONTRACT_MANAGE: 5,
  PRIVATE_COMPUTE: 6,
  DPOS_ERC20: 7,
  DPOS_STAKE: 8,
  SUBSCRIBE_MANAGE: 9,
  ARCHIVE_MANAGE: 10,
  CROSS_TRANSACTION: 11,
  PUBKEY_MANAGE: 12,
  ACCOUNT_MANAGER: 13,
  RELAY_CROSS: 17,
  TRANSACTION_MANAGER: 18,
  T: 99
};

goog.object.extend(exports, proto.syscontract);
