import * as jspb from 'google-protobuf'



export enum CertManageFunction { 
  CERT_ADD = 0,
  CERTS_DELETE = 1,
  CERTS_QUERY = 2,
  CERTS_FREEZE = 3,
  CERTS_UNFREEZE = 4,
  CERTS_REVOKE = 5,
  CERT_ALIAS_ADD = 6,
  CERT_ALIAS_UPDATE = 7,
  CERTS_ALIAS_DELETE = 8,
  CERTS_ALIAS_QUERY = 9,
}
