import * as jspb from 'google-protobuf'



export enum DPoSERC20Function { 
  GET_OWNER = 0,
  GET_DECIMALS = 1,
  TRANSFER = 2,
  TRANSFER_FROM = 3,
  GET_BALANCEOF = 4,
  APPROVE = 5,
  GET_ALLOWANCE = 6,
  BURN = 7,
  MINT = 8,
  TRANSFER_OWNERSHIP = 9,
  GET_TOTAL_SUPPLY = 10,
}
