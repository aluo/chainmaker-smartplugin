// source: syscontract/transaction_manager.proto
/**
 * @fileoverview
 * @enhanceable
 * @suppress {missingRequire} reports error on implicit type usages.
 * @suppress {messageConventions} JS Compiler reports an error if a variable or
 *     field starts with 'MSG_' and isn't a translatable message.
 * @public
 */
// GENERATED CODE -- DO NOT EDIT!
/* eslint-disable */
// @ts-nocheck

var jspb = require('google-protobuf');
var goog = jspb;
var global = (function() { return this || window || global || self || Function('return this')(); }).call(null);

goog.exportSymbol('proto.syscontract.TransactionManagerFunction', null, global);
/**
 * @enum {number}
 */
proto.syscontract.TransactionManagerFunction = {
  ADD_BLACKLIST_TX_IDS: 0,
  DELETE_BLACKLIST_TX_IDS: 1,
  GET_BLACKLIST_TX_IDS: 2
};

goog.object.extend(exports, proto.syscontract);
