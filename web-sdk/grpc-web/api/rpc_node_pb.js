// source: api/rpc_node.proto
/**
 * @fileoverview
 * @enhanceable
 * @suppress {missingRequire} reports error on implicit type usages.
 * @suppress {messageConventions} JS Compiler reports an error if a variable or
 *     field starts with 'MSG_' and isn't a translatable message.
 * @public
 */
// GENERATED CODE -- DO NOT EDIT!
/* eslint-disable */
// @ts-nocheck

var jspb = require('google-protobuf');
var goog = jspb;
var global = (function() { return this || window || global || self || Function('return this')(); }).call(null);

var common_request_pb = require('../common/request_pb.js');
goog.object.extend(proto, common_request_pb);
var common_result_pb = require('../common/result_pb.js');
goog.object.extend(proto, common_result_pb);
var config_local_config_pb = require('../config/local_config_pb.js');
goog.object.extend(proto, config_local_config_pb);
var config_log_config_pb = require('../config/log_config_pb.js');
goog.object.extend(proto, config_log_config_pb);
var config_chainmaker_server_pb = require('../config/chainmaker_server_pb.js');
goog.object.extend(proto, config_chainmaker_server_pb);
var google_api_annotations_pb = require('../google/api/annotations_pb.js');
goog.object.extend(proto, google_api_annotations_pb);
var txpool_transaction_pool_pb = require('../txpool/transaction_pool_pb.js');
goog.object.extend(proto, txpool_transaction_pool_pb);
