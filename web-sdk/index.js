/*
 *
 *  Copyright (C) THL A29 Limited, a Tencent company. All rights reserved.
 *  SPDX-License-Identifier: Apache-2.0
 *
 */

import Sdk from "./sdk";
import User from "./sdk/userInfo";
import Utils from "./utils";

export default {
  Sdk, User, Utils,
}
